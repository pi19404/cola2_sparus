#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Created on Mon Mar 11 2013
@author: narcis palomeras
"""
# ROS imports
import roslib
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from auv_msgs.msg import WorldWaypointReq
from auv_msgs.msg import GoalDescriptor

if __name__ == '__main__':
    try:
        name = 'pose_req'
        rospy.init_node(name)

        # Create publisher
        pub = rospy.Publisher(
            "/cola2_control/world_waypoint_req", WorldWaypointReq)

        req = WorldWaypointReq()
        req.header.frame_id = 'girona500'

        r = rospy.Rate(10)
        i = 0
        while not rospy.is_shutdown():
            req.header.stamp = rospy.Time().now()
            req.position.north = 4.0
            req.position.east = 6.0
            req.position.depth = 1.0
            req.altitude = 1.5
            req.orientation.roll = 0.0
            req.orientation.pitch = 0.0
            req.orientation.yaw = 1.57
            req.altitude_mode = True
            req.disable_axis.x = False
            req.disable_axis.y = False
            req.disable_axis.z = False
            req.disable_axis.roll = True
            req.disable_axis.pitch = True
            req.disable_axis.yaw = False
            req.goal.requester = name
            req.goal.id = i
            req.goal.priority = GoalDescriptor.PRIORITY_NORMAL
            pub.publish(req)
            r.sleep()
            i = i + 1

    except rospy.ROSInterruptException:
        pass

