#!/usr/bin/env python

import roslib
import rospy
from cola2_lib import cola2_lib
from cola2_navigation_dev.msg import ADIS16480IMU
from numpy import *
import random
import PyKDL


class TEST_IMU_CALIBRATION:
    """ This class is able to generate data to test imu calibration """

    def __init__(self, name):
        """ Init the class """
        self.name = name

        # Init vars
        self.rate = 10
        self.iteration = 0
        self.angle = 0
        self.base_angular_speed = 0.2
        self.angular_speed_noise = 0.005
        self.magnetometer_noise = 0.005
        self.magnetometer_mod = 0.28
        self.y_axis_factor = 0.6
        self.offset = [-0.15, +0.02]
        self.rotation = -1.0
        self.rotation_noise = 0.05
        self.acceleration_noise = 0.6
        self.tilt_offset = 0.5

        # Publisher
        self.pub = rospy.Publisher('adis_16480_imu_data', ADIS16480IMU)

        # Timer for sending imu data
        rospy.Timer(rospy.Duration(1.0 / self.rate), self.iterate)


    def iterate(self, event):
        """ This method compute and sends data """
        # Compute w with noise
        angular_speed = self.base_angular_speed + random.uniform(-self.angular_speed_noise,self.angular_speed_noise)

        # Compute angle
        self.angle = cola2_lib.wrapAngle(self.angle + angular_speed / self.rate)

        # Compute x and y
        x = self.magnetometer_mod * math.cos(self.angle) + random.uniform(-self.magnetometer_noise,self.magnetometer_noise)
        y = self.magnetometer_mod * math.sin(self.angle) + random.uniform(-self.magnetometer_noise,self.magnetometer_noise)

        # Comute y axis scaling
        y = y * self.y_axis_factor

        # Add offset with noise
        x = x + self.offset[0]
        y = y + self.offset[1]

        # Rotate
        rotation_noise = random.uniform(-self.rotation_noise,self.rotation_noise)
        x_rot = x * math.cos(self.rotation + rotation_noise) - y * math.sin(self.rotation + rotation_noise)
        y_rot = x * math.sin(self.rotation + rotation_noise) + y * math.cos(self.rotation + rotation_noise)

        # Publish data
        msg = ADIS16480IMU()
        msg.header.stamp = rospy.Time.now()
        msg.header.frame_id = "adis_16480_imu"
        msg.AX = random.uniform(-self.acceleration_noise,self.acceleration_noise)
        msg.AY = random.uniform(-self.acceleration_noise,self.acceleration_noise)
        msg.AZ = -9.81 - random.uniform(0.0,self.acceleration_noise)
        msg.GX = 0
        msg.GY = 0
        msg.GZ = 0
        msg.MX = x_rot
        msg.MY = y_rot
        msg.MZ = 0
        self.pub.publish(msg)

        # Increment self.iteration var.
        self.iteration = self.iteration + 1


if __name__ == '__main__':
    try:
        rospy.init_node('test_imu_calibration')
        test_imu_calibration = TEST_IMU_CALIBRATION(rospy.get_name())
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
