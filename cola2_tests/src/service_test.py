#!/usr/bin/env python

# ROS imports
import roslib
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from cola2_control.srv import StringList, StringListResponse

# Python imports
from cola2_lib import cola2_lib, cola2_ros_lib, NED


class ServiceTest:
    def __init__(self, name):
        """ Init the class """
        self.name = name

        # Create services
        self.test_srv = rospy.Service('/test', StringList, self.test)


    def test(self, req):
        rospy.loginfo("%s: service recieved %s", self.name, req.mystring)
        return StringListResponse(False)


if __name__ == '__main__':
    try:
        rospy.init_node('service_test')
        service_test = ServiceTest(rospy.get_name())
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
