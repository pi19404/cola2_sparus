#!/usr/bin/env python

# ROS imports
import roslib
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from cola2_control.msg import ThrustersData

if __name__ == '__main__':
    try:
        rospy.init_node('thrusters_test')
        # Publish computed data
        # Create publisher
        pub = rospy.Publisher("/cola2_control/thrusters_data",
                                           ThrustersData)
        r = rospy.Rate(10)
        for i in range(20):
            thrusters = ThrustersData()
            thrusters.header.stamp = rospy.Time.now()
            thrusters.header.frame_id = "girona500"
            thrusters.setpoints = [0.2, 0.2, 0.2, 0.2, 0.2]
            pub.publish(thrusters)
            r.sleep()

    except rospy.ROSInterruptException: pass
