#!/usr/bin/env python
"""@@Velocity controller of Sparus II AUV.@@"""

# ROS imports
import roslib
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from auv_msgs.msg import BodyForceReq
from auv_msgs.msg import NavSts
from auv_msgs.msg import BodyVelocityReq
from auv_msgs.msg import GoalDescriptor


# Python imports
from numpy import zeros, array

# Custom imports
from cola2_lib import cola2_lib, cola2_ros_lib

class VelocityControllerS2 :
    def __init__(self, name):
        """ Controls the velocity of an AUV  """
        self.name = name

        # Load parameters
        self.get_config()

        # Goal descriptor
        self.goal = GoalDescriptor()

        # Init input data
        self.v = zeros(6)
        self.desired_velocity = zeros(6)
        self.req = BodyVelocityReq()

        # Check navigator
        self.init_navigator_check = False
        self.navigator_ok = False
        rospy.Timer(rospy.Duration(1.0), self.navigator_check)

        # Create publisher
        self.pub_tau = rospy.Publisher("/cola2_control/body_force_req",
                                       BodyForceReq)

        # Create Subscriber
        rospy.Subscriber("/cola2_navigation/nav_sts",
                         NavSts, self.update_nav_sts)
        rospy.Subscriber("/cola2_control/merged_body_velocity_req",
                         BodyVelocityReq, self.compute_request)

        # Show message
        rospy.loginfo("%s: initialized", self.name)


    def update_nav_sts(self, nav_sts):
        """ Store velocity from navigation data  """
        self.v[0] = nav_sts.body_velocity.x
        self.v[1] = nav_sts.body_velocity.y
        self.v[2] = nav_sts.body_velocity.z
        self.v[3] = nav_sts.orientation_rate.roll
        self.v[4] = nav_sts.orientation_rate.pitch
        self.v[5] = nav_sts.orientation_rate.yaw

        self.last_navigator_callback = rospy.Time.now()
        if not self.init_navigator_check:
            self.init_navigator_check = True


    def navigator_check(self, event):
        """ Timer to check navigator """
        if self.init_navigator_check:
            if (rospy.Time.now() - self.last_navigator_callback).to_sec() < 2.0:
                self.navigator_ok = True
            else:
                self.navigator_ok = False


    def compute_request(self, req):
        """ Callback: computes velocity request """
        if self.navigator_ok:
            # Store data
            self.goal = req.goal
            self.desired_velocity[0] = req.twist.linear.x
            self.desired_velocity[1] = req.twist.linear.y
            self.desired_velocity[2] = req.twist.linear.z
            self.desired_velocity[3] = req.twist.angular.x
            self.desired_velocity[4] = req.twist.angular.y
            self.desired_velocity[5] = req.twist.angular.z
            self.req = req

            #rospy.loginfo( "%s: desired velocity: %s",
            #               self.name, str(self.desired_velocity) )
            #rospy.loginfo( "%s: current velocity: %s",
            #               self.name, str(self.v) )

            # Main loop
            # Compute TAU using a PID
            pid_tau  = self.pid.compute(self.desired_velocity - self.v, zeros(6),
                                        rospy.Time.now().to_sec())
            #rospy.loginfo("%s: raw pid tau: %s", self.name, str(pid_tau))

            # Compute TAU using an Open Loop Controller
            open_loop_tau = zeros(6)
            for i in range(6):
                open_loop_tau[i] = cola2_lib.polyval(self.adjust_poly[i],
                                                     self.desired_velocity[i])

            # Combine open loop TAU and PID TAU and adjust to max_force (Newtons & Nm)
            tau = cola2_lib.saturateValue(pid_tau + open_loop_tau, 1.0) * self.force_max

            #rospy.loginfo("%s: tau: %s", self.name, str(tau))

            data = BodyForceReq()
            data.goal = self.goal
            data.goal.requester = self.name
            data.header.stamp = rospy.Time.now()
            data.header.frame_id = "sparus2"

            # SURGE
            data.disable_axis.x = self.req.disable_axis.x
            if data.disable_axis.x:
                data.wrench.force.x = 0.0
                self.pid.resetDof(0)
            else:
                data.wrench.force.x = tau[0]

            # SWAY
            data.disable_axis.y = True
            data.wrench.force.y = 0.0

            # HEAVE
            data.disable_axis.z = self.req.disable_axis.z
            if data.disable_axis.z:
                data.wrench.force.z = 0.0
                #self.pid.resetDof(2)
            else:
                data.wrench.force.z = tau[2]

            # ROLL
            data.disable_axis.roll = True
            data.wrench.torque.x = 0.0

            # PITCH
            data.disable_axis.pitch = self.req.disable_axis.pitch
            if data.disable_axis.pitch:
                data.wrench.torque.y = 0.0
                self.pid.resetDof(4)
            else:
                data.wrench.torque.y = tau[4]

            # YAW
            data.disable_axis.yaw = self.req.disable_axis.yaw
            if data.disable_axis.yaw:
                data.wrench.torque.z = 0.0
                self.pid.resetDof(5)
            else:
                data.wrench.torque.z = tau[5]

            # Publish data
            #rospy.loginfo("%s: tau: %s", self.name, data.wrench)
            self.pub_tau.publish(data)

        else:
            self.pid.reset(rospy.Time.now().to_sec())


    def get_config(self) :
        """ Load parameters from the rosparam server """
        param_dict = {'force_max': 'velocity_controller/force_max',
                      'fff': 'velocity_controller/pid_velocity_feed_forward_force',
                      'kp': 'velocity_controller/pid_velocity_kp',
                      'ti': 'velocity_controller/pid_velocity_ti',
                      'td': 'velocity_controller/pid_velocity_td',
                      'i_limit': 'velocity_controller/pid_velocity_i_limit',
                      'open_loop_adjust_poly_x': 'velocity_controller/open_loop_adjust_poly_x',
                      'open_loop_adjust_poly_y': 'velocity_controller/open_loop_adjust_poly_y',
                      'open_loop_adjust_poly_z': 'velocity_controller/open_loop_adjust_poly_z',
                      'open_loop_adjust_poly_roll': 'velocity_controller/open_loop_adjust_poly_roll',
                      'open_loop_adjust_poly_pitch': 'velocity_controller/open_loop_adjust_poly_pitch',
                      'open_loop_adjust_poly_yaw': 'velocity_controller/open_loop_adjust_poly_yaw'}

        if not cola2_ros_lib.getRosParams(self, param_dict, self.name):
            rospy.logfatal("%s: shutdown due to invalid config parameters!", self.name)
            exit(0)  # TODO: find a better way

        # PID Close loop parameters
        self.force_max = array(self.force_max)
        self.fff = array(self.fff)
        self.kp = array(self.kp)
        self.ti = array(self.ti)
        self.td = array(self.td)
        self.i_limit = array(self.i_limit)
        self.pid = cola2_lib.PID(self.kp, self.ti, self.td, self.fff,
                                 rospy.Time.now().to_sec(), self.i_limit)

        # Open loop model parameters
        self.adjust_poly = []
        self.adjust_poly.append(self.open_loop_adjust_poly_x)
        self.adjust_poly.append(self.open_loop_adjust_poly_y)
        self.adjust_poly.append(self.open_loop_adjust_poly_z)
        self.adjust_poly.append(self.open_loop_adjust_poly_roll)
        self.adjust_poly.append(self.open_loop_adjust_poly_pitch)
        self.adjust_poly.append(self.open_loop_adjust_poly_yaw)


if __name__ == '__main__':
    try:
        rospy.init_node('velocity_controller_s2')
        __velocity_controlle__r = VelocityControllerS2(rospy.get_name())
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
