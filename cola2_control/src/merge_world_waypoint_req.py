#!/usr/bin/env python

"""
Created on 2013 updated on 31/03/2014
@author: Narcis Palomeras
"""

# ROS imports
import roslib
roslib.load_manifest('cola2_control')
import rospy

# Python imports
import copy

# Msgs imports
from auv_msgs.msg import WorldWaypointReq, NavSts
from cola2_safety.srv import RecoveryAction, RecoveryActionRequest

# COLA2 imports
import merge
from cola2_lib import cola2_ros_lib

class MergeWorldWaypointReq:
    """@@This node is used to merge world_waypoint_req messages, 
         taking into account message priorities and disabled axis.@@"""
 
    def __init__(self, name):
        """ Constructor for class MergeBodyVelocityReq."""
        self.name = name
        self.world_waypoint_requests = list()
        self.frame_id = "/vehicle"
        self.total_depth = 10
        self.last_total_depth = rospy.Time.now().to_sec()
        self.nav_init = False
        
        # Get config
        param_dict = {'frame_id': 'merge/frame_id'}
        cola2_ros_lib.getRosParams(self, param_dict)

        # Initialize response        
        self.response = WorldWaypointReq()
        self.response.header.frame_id = self.frame_id
        self.last_response = copy.deepcopy(self.response)

        # Create publisher
        self.pub_coordinated = rospy.Publisher(
            "/cola2_control/merged_world_waypoint_req", WorldWaypointReq)

        # Create subscriber
        rospy.Subscriber(
               "/cola2_control/world_waypoint_req", WorldWaypointReq,
               self.update_response)

        rospy.Subscriber(
               "/cola2_navigation/nav_sts", NavSts,
               self.update_nav)

        
    def update_response(self, resp):
        """ Receive WorldWaypointReq messages and adds it in sp list"""
        
        # World Waypoint Req can be refeneced to depth or altitude.
        # In order to simplify the control, all requests will be 
        # re-referenced to depth.
        if resp.altitude_mode:
            # First check if altitude is valid or not and abort if necessary.
            if (rospy.Time.now().to_sec() - self.last_total_depth) > 60.0:
                rospy.logfatal('%s: ABORT MISSION! Invalid altitude!', 
                               self.name)
                try:
                    rospy.wait_for_service(
                        '/cola2_safety/recovery_action', 20)
                    recover_action_srv = rospy.ServiceProxy(
                        '/cola2_safety/recovery_action', RecoveryAction)
                    recover_action_srv(
                        RecoveryActionRequest.ABORT_AND_SURFACE)
                except rospy.exceptions.ROSException:
                    rospy.logerr(
                        '%s: error creating client to recovery action', 
                        self.name)
                    rospy.signal_shutdown('/merge_world_waypoint_req: error '
                                          'creating recover action client')
    
            elif self.nav_init:
                # If altitude is updated and some nav msg has arrived
                # transform altitude into depth
                resp.position.depth = self.total_depth - resp.altitude
                if resp.position.depth < 0.0:
                    resp.position.depth = 0.0
                resp.altitude_mode = False
            else:
                # If navigation has not yet started, disable Z axis
                resp.disable_axis.z = True

        # Add request to requests list
        merge.update_response(self.world_waypoint_requests, resp)

        # Merge requests 
        [value, disable_axis, goal] = merge.iterate(
                self.world_waypoint_requests, self.name)
        self.response.header.stamp = rospy.Time().now()
        self.response.position.north = value[0]
        self.response.position.east = value[1]
        self.response.position.depth = value[2]
        self.response.altitude = value[2]
        self.response.orientation.roll = value[3]
        self.response.orientation.pitch = value[4]
        self.response.orientation.yaw = value[5]
        self.response.altitude_mode = False
        self.response.disable_axis = disable_axis
        self.response.goal = goal
        
        if not merge.are_world_waypoint_req_equal(self.response, 
                                                  self.last_response):
            self.pub_coordinated.publish(self.response)
            self.last_response = copy.deepcopy(self.response)
            
                
    def update_nav(self, nav):
        self.nav_init = True
        if nav.altitude > 0:
            self.total_depth = nav.altitude + nav.position.depth
            self.last_total_depth = rospy.Time.now().to_sec()
        else:
            self.total_depth = nav.position.depth + 0.5
    

if __name__ == '__main__':
    try:
        rospy.init_node('merge_world_waypoint_req')
        MWWR = MergeWorldWaypointReq(rospy.get_name())
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
