#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 25 2013

@author: Narcis Palomeras
"""
# ROS imports
import roslib 
roslib.load_manifest('udg_pandora')
import rospy

from udg_pandora.msg import Point2DMatched
from geometry_msgs.msg import PoseWithCovarianceStamped
from auv_msgs.msg import BodyVelocityReq
from auv_msgs.msg import GoalDescriptor
import cola2_lib
import PyKDL
import numpy as np
import random 

class IBVS:
    def __init__(self, name):
        self.name = name
        self.depth_defined = False
        self.depth = 1.0
        # Camera axis
        self.available_axis = [True, True, False, False, False, False]
        self.distance_to_image = 0.7
                
        # Camera intrinsic parameters
        self.f = 0.08
        self.pu = 0.00696 #0.00696
        self.pv = 0.0089 #0.0089
        self.u0 = 320
        self.v0 = 240

        # PID controller
        now = rospy.Time.now().to_sec()
        self.controller = cola2_lib.PID([0.0025, 0.0025, 0.05, 0.0, 0.0, 0.9], # Kp
                                        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],     # Ti
                                        [0.2, 0.005, 0.005, 0.0, 0.0, 0.0], # Kd
                                        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],     # fff                               
                                        now)
        
        # Camera frame wrt vehicle frame
        self.R = PyKDL.Rotation.RPY(0.0, 0.0, 1.57)
        self.t = np.array([0.5, -0.06, 0.4])
        
        # For test only
        random.seed()
        
        # Create publisher
        self.pub_tau = rospy.Publisher("/cola2_control/body_velocity_req", 
                               BodyVelocityReq)
                                       
        # Create subscribers
        rospy.Subscriber('/visual_detector/pixel_matches', 
                         Point2DMatched, 
                         self.computeMovement)
        rospy.Subscriber('/pose_ekf_slam/landmark_update/panel_centre', 
                        PoseWithCovarianceStamped, 
                        self.saveDepth)
                
                       
    def computeMovement(self, data):
        if self.depth_defined:
            # Compute interaction matrix for each pixel pair 
            accum = np.zeros((0, 6))
            vector = np.array([])
            for i in range(len(data.img0)): # len(data.img0)
                px = (data.img0[i].x - self.u0) - (data.img1[i].x - self.u0)
                py = (data.img0[i].y - self.v0) - (data.img1[i].y - self.v0)
                J = self.computeImageJacobian(px, py)
                accum = np.concatenate((accum, J), axis=0)
                vector = np.concatenate((vector,[px, py]))
            
            # Compute camera speed            
            velocity = np.matrix(accum).I * np.matrix(vector).T
            print 'velocity: \n', velocity
            
            # Transform speed from camera frame to vehicle frame
            # TODO: Distance to the features is not computed by IBVS
            vc = PyKDL.Vector(velocity[0,0], velocity[1,0], self.depth - self.distance_to_image)
            wc = PyKDL.Vector(velocity[3,0], velocity[4,0], velocity[5,0])
            wv = self.R * wc
            tmp = np.cross(np.array([wv[0], wv[1], wv[2]]), self.t)
            vv = self.R*vc - PyKDL.Vector(tmp[0], tmp[1], tmp[2])

            # Compute control law
            desired = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
            current = np.array([vv[0],
                                vv[1],
                                vv[2],
                                wv[0],
                                wv[1],
                                wv[2]])
            print 'current velocity in vehicle frame: \n', current 
            tau = self.controller.compute(current, desired, 
                                          rospy.Time.now().to_sec())
            print 'Tau: \n', tau
            
            # Put values near zero to zero
            for i in range(6):
                if abs(tau[i]) < 0.005:
                    tau[i] = 0.0
                    
            self.sendControlAction(tau)


    def sendControlAction(self, tau):

        data = BodyVelocityReq()
        data.goal.requester = self.name
        data.goal.priority = GoalDescriptor.PRIORITY_NORMAL
        data.header.stamp = rospy.Time.now()
        data.header.frame_id = "girona500"
       
        # Surge
        data.disable_axis.x = False
        data.twist.linear.x = tau[0]
        
        # Sway
        data.disable_axis.y = not self.available_axis[0]
        data.twist.linear.y = tau[1] 
        
        # Heave
        data.disable_axis.z = not self.available_axis[1]
        data.twist.linear.z = tau[2] 
        
        # Yaw
        data.disable_axis.yaw = not self.available_axis[4]
        data.twist.angular.z = tau[5] 
        
        print 'Desired action: \n', data
        
        self.pub_tau.publish(data)
                
        
    def test(self):
        template_x = [300, 340, 300, 340]
        template_y = [220, 260, 220, 260]
        current_x = [0, 0, 0, 0]
        current_y = [0, 0, 0, 0]
        
        # center template to u0 and v0
        for i in range(len(template_x)):
            template_x[i] = template_x[i] - self.u0
            template_y[i] = template_y[i] - self.v0
        
        # displace x and y
        x_displacement = 20
        y_displacement = -10
        for i in range(len(template_x)):
            current_x[i] = template_x[i] + x_displacement
            current_y[i] = template_y[i] + y_displacement
        
        # rotate current image
        angle = 0 # np.pi/8.0
        rotation_matrix = np.zeros((2,2))
        rotation_matrix[0, 0] = np.cos(angle)
        rotation_matrix[0, 1] = -np.sin(angle)
        rotation_matrix[1, 0] = np.sin(angle)
        rotation_matrix[1, 1] = np.cos(angle)
        
        # add noise
        max_pixel_error = 3
        for i in range(len(template_x)):
            current_x[i] = current_x[i] + round(random.random() * max_pixel_error * 2) - max_pixel_error
            current_y[i] = current_y[i] + round(random.random() * max_pixel_error * 2) - max_pixel_error
            
        for i in range(len(template_x)):
            point = np.zeros((2,1))
            point[0,0] = current_x[i]
            point[1,0] = current_y[i]
            rotated_point = np.matrix(rotation_matrix) * point
            current_x[i] = rotated_point[0,0]
            current_y[i] = rotated_point[1,0]
            
        # Compute movement
        accum = np.zeros((0, 6))
        vector = np.array([])

        for i in range(4):
            px = template_x[i] - current_x[i]
            py = template_y[i] - current_y[i]
            J = self.computeImageJacobian(px, py)
            accum = np.concatenate((accum, J), axis=0)
            vector = np.concatenate((vector,[px, py]))
        
        # Compute pseudo-inverse
        velocity = np.matrix(accum).I * np.matrix(vector).T
        nice_vel = [ '%.2f' % elem for elem in velocity ]
        print 'velocity: \n', nice_vel
        print 'vector: \n', vector
        
        
    def saveDepth(self, data):
        self.depth_defined = True
        self.depth = data.pose.pose.position.z

  
    def computeImageJacobian(self, u, v):
        ret = np.zeros((2,6))
        ret[0,0] = -self.f/(self.pu*self.depth)
        ret[0,2] = u/self.depth
        ret[0,3] = self.pu*u*v/self.f 
        ret[0,4] = -(self.f**2 + self.pu**2 * u**2)/(self.pu * self.f)
        ret[0,5] = v
        ret[1,1] = -self.f/(self.pv*self.depth)
        ret[1,2] = v/self.depth        
        ret[1,3] = (self.f**2 + self.pv**2 * v**2)/(self.pv * self.f)
        ret[1,4] = -self.pv*u*v/self.f
        ret[1,5] = -u
        
        for i in range(6):
            if not self.available_axis[i]:
                ret[0,i] = 0.0
                ret[1,i] = 0.0
                
        return ret
    
    
if __name__ == '__main__':
    try:
        # Init node
        rospy.init_node('image_based_visual_servoing')
        ibvs = IBVS(rospy.get_name())
        # ibvs.test()
        rospy.spin()
    except rospy.ROSInterruptException: 
        pass

