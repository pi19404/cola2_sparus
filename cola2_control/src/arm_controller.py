#!/usr/bin/env python

# ROS imports
import roslib
roslib.load_manifest('cola2_control')
import rospy
#use to load the configuration function
#is needed the from if we do the import
import cola2_lib
from cola2_lib import cola2_ros_lib

#import empty service to set the 0 position and the valve position
#from std_srvs.srv import Empty, EmptyResponse

import numpy as np

from sensor_msgs.msg import JointState, Joy
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import PoseWithCovarianceStamped

#Service Import
from cola2_control.srv import ComputeInverseKinematics, ComputeInverseKinematicsResponse

from cola2_control.srv import JointPose, JointPoseResponse
from cola2_control.srv import EFPose, EFPoseResponse

from cola2_control.srv import TurnDesiredDegrees, TurnDesiredDegreesResponse
from std_srvs.srv import Empty, EmptyResponse

from cola2_control.msg import Detection

from std_msgs.msg import Float64

#Import library to compute FK Jac and IK

from PyKDL import Chain, Joint, Frame, Segment, ChainFkSolverPos_recursive
from PyKDL import JntArray, ChainJntToJacSolver, Jacobian
from PyKDL import diff, Rotation, Vector

from cola2_control.msg import VelocityAndSpeed

#header
#string[] name
#float64[] position
#float64[] velocity
#float64[] effort

import threading

import tf
#import ipdb
#ipdb.set_trace()


class armControllerDev:

    def __init__(self, name):
        self.name = name
        self.getConfig()
        rospy.loginfo('Configuration ' + str(self.name) + ' Loaded ')
        self.initArmDH()
        rospy.loginfo('Arm Parameters Loaded and chain model created')
        self.fk_solver = ChainFkSolverPos_recursive(self.chain)
        self.jac_solver = ChainJntToJacSolver(self.chain)
        self.useJacobian = np.zeros([3, 3])
        self.current_state = np.zeros(self.numDoF)
        #X,Y,Z,ROLL,PITCH,YAW
        self.current_pose_ef = np.zeros(6)
        #Joint coef extracted from the geometry of the arm
        self.joint_coef = np.zeros(3)

        #Locks for the data
        self.lock_joint_state = threading.Lock()
        self.lock_comand_volt = threading.Lock()
        self.lock_comand_jnt = threading.Lock()
        self.lock_comand_pose = threading.Lock()
        self.lock_comand_vel = threading.Lock()

        #Booleans to make a selector depending on the commands
        self.volt_control = False
        self.jnt_control = False
        self.pose_control = False
        self.vel_control = False
        self.srv_control = False

        self.new_roll_vel = False

        # A boolean to move the arm only if there are new commands in the
        # control when is used direct control
        #self.new_data = False

        #Initialize vector to safe each command
        self.des_q_vlt = np.zeros(5)
        self.des_q_jnt = np.ones(5)*99
        self.des_q_pose = np.ones(5)*99
        self.des_q_vel = np.zeros(5)

        #Previous Orientation
        self.init_ori_hand = False
        self.initial_ori = np.zeros(3)

        #Create a tflistener to transform the camera position into the world position
        self.tflistener = tf.TransformListener()

        #Counter to initialize the Control with derivative
        self.joints_updates = 0
        self.initialize = False
        self.time = rospy.get_time()
        #Publish the desired command
        self.pub_joint_command = rospy.Publisher(
            '/csip_e5_arm/joint_voltage_command',
            JointState)
        #publish the current position of the arm
        self.pub_fk = rospy.Publisher(
            '/arm/pose_stamped',
            PoseStamped)
        #publish the position of the valve detecte by the end-effector
        self.pub_landmark = rospy.Publisher(
            '/pose_ekf_slam/landmark_update/valve_1',
            PoseWithCovarianceStamped)

        self.pub_vel_speed_ee = rospy.Publisher(
            '/arm/velocity_speed_ee',
             VelocityAndSpeed)

        #Subscribe to the desired position
        rospy.Subscriber('/csip_e5_arm/joint_state',
                         JointState, self.update_joint_state)
        #Subscribe to the jnt command
        rospy.Subscriber('/cola2_control/joystick_arm_volt',
                         Joy, self.update_open_loop_command)
        #Subscribe to the jnt command
        rospy.Subscriber('/cola2_control/joystick_arm_jnt',
                         Joy, self.update_joint_command)
        #Subscribe to the desired position
        rospy.Subscriber('/cola2_control/joystick_arm_ef',
                         Joy, self.update_pose_command)
        #Subscribe to the desired position
        rospy.Subscriber('/cola2_control/joystick_arm_ef_vel',
                         Joy, self.update_velocity_command)
        #Subscribe to the Coef computed in the CSIP
        rospy.Subscriber("/csip_e5_arm/joint_state_coef",
                         JointState, self.update_joint_coef)

        rospy.Subscriber('/cola2_control/roll_command',
                         Float64, self.update_roll_command)

        #Subscribe to the Position of the valve from the end-effector
        rospy.Subscriber('/visual_detector/endeffector_valve',
                         Detection, self.update_valve_pose)

        self.serv_ik = rospy.Service(
            '/cola2_control/computeInverseKinematics',
            ComputeInverseKinematics,
            self.handle_IK)

        self.jointSrv = rospy.Service(
            '/cola2_control/setJointPose',
            JointPose,
            self.set_joint_pose)

        self.poseEFSrv = rospy.Service(
            '/cola2_control/setPoseEF',
            EFPose,
            self.set_ef_pose)

        self.turn90Srv = rospy.Service(
            '/cola2_control/turn90Degrees',
            Empty,
            self.turn_90_degres)

        self.turnDesiredDegSrv = rospy.Service(
            '/cola2_control/turnDesiredDegrees',
            TurnDesiredDegrees,
            self.turn_desired_degrees)

        self.turnDesiredRadSrv = rospy.Service(
            '/cola2_control/turnDesiredRadians',
            TurnDesiredDegrees,
            self.turn_desired_radians)


        # fixed distances for Inverse Kinematics
        self.IK_l = np.sqrt((self.DH_a3**2) + (self.DH_d4**2))
        self.IK_alpha = np.arctan2(self.DH_d4, self.DH_a3)

        # Created a broadcaster for the TK of each joint of the arm
        self.br = tf.TransformBroadcaster()

    def getConfig(self):
        param_dict = {'period': '/arm_controller/period',
                      'kq': '/arm_controller/kq',
                      'kdq': '/arm_controller/kdq',
                      'dt': '/arm_controller/dt',
                      'numDoF': '/arm_controller/numDoF',
                      'maxSaturate': '/arm_controller/maxSaturate',
                      'maxVoltatge': '/arm_controller/maxVoltatge',
                      'minNegVolRoll': '/arm_controller/minNegVolRoll',
                      'minPosVolRoll': '/arm_controller/minPosVolRoll',
                      'DH_a1': '/arm_controller/DH_a1',
                      'DH_a2': '/arm_controller/DH_a2',
                      'DH_a3': '/arm_controller/DH_a3',
                      'DH_d4': '/arm_controller/DH_d4',
                      'DH_alpha1': '/arm_controller/DH_alpha1',
                      'DH_alpha3': '/arm_controller/DH_alpha3',
                      'max_joint': '/arm_controller/maximum_limit_joint',
                      'min_joint': '/arm_controller/minimum_limit_joint',
                      'base_pose': '/arm_controller/base_pose',
                      'base_ori': '/arm_controller/base_ori',
                      'offset_ee_camera': '/arm_controller/offset_ee_camera'
                      }
        cola2_ros_lib.getRosParams(self, param_dict)

    def initArmDH(self):
        self.chain = Chain()
        #The Fixed joint is not included
        joint0 = Joint(Joint.RotZ)
        frame0 = Frame().DH(self.DH_a1, self.DH_alpha1, 0, 0)
        segment0 = Segment(joint0, frame0)
        self.chain.addSegment(segment0)

        joint1 = Joint(Joint.RotZ)
        frame1 = Frame().DH(self.DH_a2, 0, 0, 0)
        segment1 = Segment(joint1, frame1)
        self.chain.addSegment(segment1)

        joint2 = Joint(Joint.RotZ)
        frame2 = Frame().DH(self.DH_a3, self.DH_alpha3, 0, 0)
        segment2 = Segment(joint2, frame2)
        self.chain.addSegment(segment2)

        joint3 = Joint(Joint.RotZ)
        frame3 = Frame().DH(0, 0, self.DH_d4, 0)
        segment3 = Segment(joint3, frame3)
        self.chain.addSegment(segment3)

    #move the arm without using the calibration
    def update_open_loop_command(self, data):
        #rospy.loginfo('Update Open Loop Command')
        if not self.srv_control:
            self.lock_comand_volt.acquire()
            try:
                self.volt_control = True
                self.jnt_control = False
                self.pose_control = False
                self.vel_control = False
                self.des_q_vlt = np.array([
                    data.axes[1], -1*data.axes[2], data.axes[0],
                    data.axes[5], 0.0])
            finally:
                self.lock_comand_volt.release()

    def update_joint_command(self, data):
        #rospy.loginfo('Update Joint Command')
        if not self.srv_control:
            self.lock_comand_jnt.acquire()
            try:
                input_vect = np.array([data.axes[1], -1*data.axes[2],
                                       data.axes[0],
                                       data.axes[5], 0.0])
                if any(input_vect != 0.0):
                    self.volt_control = False
                    self.jnt_control = True
                    self.pose_control = False
                    self.vel_control = False
                    for i in xrange(self.numDoF):
                        if input_vect[i] != 0.0:
                            # self.des_q_jnt[i] = cola2_lib.wrapAngle(self.current_state[i]
                            #                                         + input_vect[i])
                            self.des_q_jnt[i] = self.current_state[i] + input_vect[i]
                            # if i == 3:
                            #     rospy.loginfo('Current State ' + str(self.current_state[i]) +
                            #                   'Desired State ' + str(self.des_q_jnt[i]))
                            #     rospy.loginfo('****************************************')
                        else:
                            #If the vector has been intialize
                            #in the first command
                            #we set in the current state
                            if self.des_q_jnt[i] == 99:
                                self.des_q_jnt = self.current_state
                            #else des_q has been change one
                            #time we keep the value
                else:
                    self.volt_control = False
                    self.jnt_control = False
                    self.pose_control = False
                    self.vel_control = False
            finally:
                self.lock_comand_jnt.release()

    def update_pose_command(self, data):
        #rospy.loginfo('Update Pose Command')
        if not self.srv_control:
            self.lock_comand_pose.acquire()
            try:
                if any(np.asarray(data.axes[0:6]) != 0.0):
                    desired_pose_ef = self.current_pose_ef + data.axes[0:6]
                    desired_pose_ef[3] = cola2_lib.wrapAngle(self.current_state[3] + data.axes[5])
                    # rospy.loginfo('Desired Pose ' + str(desired_pose_ef[3]))
                    # rospy.loginfo('Current Roll ' +
                    #               str(self.current_pose_ef[3]))
                    # #+ ' Diference ' + str(data.axes[3]))
                    # rospy.loginfo('****************************************')
                    self.volt_control = False
                    self.jnt_control = False
                    self.pose_control = True
                    self.vel_control = False

                    ik = self.computeInverseKinematics(desired_pose_ef)
                    #rospy.loginfo('Ik solve ' + str(ik[3]))
                    if (all(ik != -1) and all(ik != -2) and all(ik != -3)):
                        for i in xrange(self.numDoF):
                            if ik[i] != self.current_state[i]:
                                self.des_q_pose[i] = ik[i]
                            else:
                                if self.des_q_jnt[i] == 99:
                                    self.des_q_pose[i] = self.current_state[i]
                    else:
                        rospy.logerr(
                            'Error computing the IK in the joint '
                            + str(-1*ik[0]))
                        self.des_q_pose = self.current_state
                    #rospy.loginfo('Desired q pose ' + str(self.des_q_pose[3]))
                else:
                    self.volt_control = False
                    self.jnt_control = False
                    self.pose_control = False
                    self.vel_control = False
            finally:
                self.lock_comand_pose.release()

    def update_velocity_command(self, data):
        #rospy.loginfo('Update Velocity Command')
        if not self.srv_control:
            self.lock_comand_vel.acquire()
            try:
                self.volt_control = False
                self.jnt_control = False
                self.pose_control = False
                self.vel_control = True
                #EndEffector Velocity
                #invJac = np.linalg.inv(self.useJacobian)
                invJac = np.linalg.pinv(self.useJacobian)
                vel = data.axes[0:3]
                qvelctrl = np.dot(invJac, vel)
                for i in xrange(3):
                    try:
                        self.des_q_vel[i] = int(
                            qvelctrl[i]*self.joint_coef[i]*self.maxVoltatge)
                    except ValueError:
                        rospy.loginfo('Error in the Update velocity')
                        rospy.loginfo('QvelCtrl Error ' + str(qvelctrl[i]))
                        rospy.loginfo('Use Jacobian ' + str(self.useJacobian))
                        rospy.loginfo('Inv Use Jacobian ' + str(invJac))
                        rospy.loginfo('vel ' + str(data.axes[0:3]))
                        self.des_q_vel[i] = 0
                    except:
                        rospy.loginfo('Error in the Update velocity')
                        rospy.loginfo('QvelCtrl Error ' + str(qvelctrl[i]))
                        rospy.loginfo('Use Jacobian ' + str(self.useJacobian))
                        rospy.loginfo('Inv Use Jacobian ' + str(invJac))
                        rospy.loginfo('vel ' + str(data.axes[0:3]))
                        self.des_q_vel[i] = 0
                                      
                #self.des_q_vel[3] = cola2_lib.wrapAngle(self.current_state[3] + data.axes[3])
                self.des_q_vel[3] = self.current_state[3] + data.axes[5]
                #rospy.loginfo('Current : ' + str(self.current_state[3]) + ' Added :' + str(data.axes[3]) + ' Result ' + str(self.des_q_vel[3]))
                self.des_q_vel[4] = 0
                self.new_roll_vel = True
            finally:
                self.lock_comand_vel.release()

    def update_roll_command(self, roll):
        self.lock_comand_vel.acquire()
        try:
            self.srv_control = False
            self.volt_control = True
            self.jnt_control = False
            self.pose_control = False
            self.vel_control = False
            self.des_q_vlt[0] = 0.0 #self.current_state[0]
            self.des_q_vlt[1] = 0.0 #self.current_state[1]
            self.des_q_vlt[2] = 0.0 #self.current_state[2]
            self.des_q_vlt[3] = roll.data
            self.des_q_vlt[4] = 0.0
        finally:
            self.lock_comand_vel.release()

    def update_joint_state(self, data):
        self.lock_joint_state.acquire()
        try:
            self.joints_updates
            self.previous_state = np.copy(self.current_state)
            self.current_state = np.asarray(data.position)
            if self.joints_updates < 2:
                self.joints_updates += 1
            else:
                self.initialize = True
        finally:
            self.lock_joint_state.release()

    def update_joint_coef(self, data):
        self.lock_joint_state.acquire()
        try:
            self.joint_coef[0] = data.position[0]
            self.joint_coef[1] = data.position[1]
            self.joint_coef[2] = data.position[2]
        finally:
            self.lock_joint_state.release()

    def update_valve_pose(self, data):
        """
        This function read the distance between the camera in the hand and the valve and plot it.
        """
#        if data.detected:
        try:
                # trans, rot = self.tflistener.lookupTransform(
                #     "world", "end_effector",
                #     self.tflistener.getLatestCommonTime(
                #         "world", "end_effector"))
                # point = np.array([data.position.position.x,
                #                   data.position.position.y,
                #                   data.position.position.z])
                # rot_matrix = tf.transformations.quaternion_matrix(rot)
                # point_rot = np.dot(rot_matrix[:3, :3], point)
                # point_trans = trans + point_rot
                # world_point = point_trans
                # #world_point = np.dot(rot_matrix[:3, :3], point_trans)
                # valve = PoseWithCovarianceStamped()
                # valve.pose.pose.position.x = world_point[0]
                # valve.pose.pose.position.y = world_point[1]
                # valve.pose.pose.position.z = world_point[2]
                # #orientation of the vavle
                # #TODO It works but for what I understand I don't know why
                # yaw = np.deg2rad(90)
                # pitch = np.deg2rad(0)
                # roll = np.deg2rad(-180)
                # rot_matrix_ee = tf.transformations.euler_matrix(
                #     yaw, pitch, roll)
                # final_rot = np.dot(rot_matrix_ee, rot_matrix)

                # quat = tf.transformations.quaternion_from_matrix(final_rot)
                # valve.pose.pose.orientation.x = quat[0]
                # valve.pose.pose.orientation.y = quat[1]
                # valve.pose.pose.orientation.z = quat[2]
                # valve.pose.pose.orientation.w = quat[3]
                # self.pub_landmark.publish(valve)
            if (data.position.position.x == 0 and
                data.position.position.y == 0 and
                data.position.position.z == 0 and
                data.position.orientation.x == 0 and
                data.position.orientation.y == 0 and
                data.position.orientation.z == 0 and
                data.position.orientation.w == 1):
                pass
            else:
                if data.detected:
                    # self.br.sendTransform(
                    #     (data.position.position.x,
                    #      data.position.position.y,
                    #      data.position.position.z),
                    #     (data.position.orientation.x,
                    #      data.position.orientation.y,
                    #      data.position.orientation.z,
                    #      data.position.orientation.w),
                    #     rospy.Time.now(),
                    #     'ee_valve_pose_ori',
                    #     'g500/hand_camera')
                    self.br.sendTransform(
                        (data.position.position.x,
                         data.position.position.y,
                         data.position.position.z),
                        (data.position.orientation.x,
                         data.position.orientation.y,
                         data.position.orientation.z,
                         data.position.orientation.w),
                        rospy.Time.now(),
                        'ee_valve_pose_ori',
                        'camera')
                else:
                    # self.br.sendTransform(
                    #     (data.position.position.x,
                    #      data.position.position.y,
                    #      data.position.position.z),
                    #     (data.position.orientation.x,
                    #      data.position.orientation.y,
                    #      data.position.orientation.z,
                    #      data.position.orientation.w),
                    #     rospy.Time.now(),
                    #     'ee_valve_pose',
                    #     'g500/hand_camera')
                    self.br.sendTransform(
                        (data.position.position.x,
                         data.position.position.y,
                         data.position.position.z),
                        (data.position.orientation.x,
                         data.position.orientation.y,
                         data.position.orientation.z,
                         data.position.orientation.w),
                        rospy.Time.now(),
                        'ee_valve_pose',
                        'camera')

        except tf.Exception:
            rospy.logerr(
                'Error reading the Tranformation from world to EE')

    def handle_IK(self, req):
        ans_IK = self.computeInverseKinematics(np.asarray(req.desired_pose))
        resp = ComputeInverseKinematicsResponse()
        if (all(ans_IK != -1) and all(ans_IK != -2) and all(ans_IK != -3)):
            resp.success = True
        else:
            resp.success = False
        #TODO: Avoid the last joint is not used for the moment.
        resp.joint_command = ans_IK[0:4].tolist()
        return resp

    def set_joint_pose(self, req):
        self.lock_comand_jnt.acquire()
        try:
            self.volt_control = False
            self.jnt_control = True
            self.pose_control = False
            self.vel_control = False
            self.des_q_jnt = np.deg2rad(req.desired_joint)
            resp = JointPoseResponse()
            resp.success = True
            self.srv_control = True
            return resp
        finally:
            self.lock_comand_jnt.release()

    def set_ef_pose(self, req):
        self.lock_comand_pose.acquire()
        try:
            resp = EFPoseResponse()
            #rospy.loginfo('Request ' + str(req.desired_pose))
            ik = self.computeInverseKinematics(req.desired_pose)
            #rospy.loginfo('Inverse Kinematics ' + str(ik))
            #rospy.loginfo('Current State ' + str(self.current_state))
            if (all(ik != -1) and all(ik != -2) and all(ik != -3)):
                self.volt_control = False
                self.jnt_control = False
                self.pose_control = True
                self.vel_control = False
                #rospy.loginfo('Ant q pose ' + str(self.des_q_pose))
                self.des_q_pose = ik
                #rospy.loginfo('Desired q pose ' + str(self.des_q_pose))
                resp.success = True
                self.srv_control = True
            else:
                resp.success = False
            return resp
        finally:
            self.lock_comand_pose.release()

    def control(self, q_desired):
        #EF_control copy
        #Compute the desired command
        # rospy.loginfo('q_desired ' + str(q_desired[3]))
        # rospy.loginfo('Current ' + str(self.current_state[3]))
        qdot_des = (self.kq*(self.current_state - q_desired) +
                    self.kdq*(self.current_state - self.previous_state)
                    / self.dt)
        qdot_des[4] = 0.0

        #rospy.loginfo('Current ' + str(self.current_state[3]))
        #rospy.loginfo('Desired ' + str(q_desired[3]))
        #saturate the values between 1 and -1
        #TODO: Check if in cola2_lib already exist this function
        for i in xrange(self.numDoF):
            if qdot_des[i] > self.maxSaturate[i]:
                qdot_des[i] = self.maxSaturate[i]
            elif qdot_des[i] < (-1*self.maxSaturate[i]):
                qdot_des[i] = -1*self.maxSaturate[i]
        #Convert to the voltatge range
        q_cmd = [int(qdot*self.maxVoltatge) for qdot in qdot_des]
        #rospy.loginfo('Values of q_cmd ' + str(q_cmd[0:4]))
        if abs(self.current_state[3] - q_desired[3]) < 0.02:
            q_cmd[3] = 0

        #Force the minimum for the Forth joint
        # The roll ones, if is not in the limit it not move
        if q_cmd[3] < self.minPosVolRoll and q_cmd[3] > 0:
            q_cmd[3] = self.minPosVolRoll
        elif q_cmd[3] > self.minNegVolRoll and q_cmd[3] < 0:
            q_cmd[3] = self.minNegVolRoll

        #negative value
        #simulator
        #q_cmd[3] = -1.0*q_cmd[3]

        #fill the message
        desJoints = JointState()
        desJoints.header.stamp = rospy.get_rostime()
        desJoints.header.frame_id = ""

        desJoints.position = q_cmd
        #desJoints.position = [0, q1cmd,  0,  0,  0]
        desJoints.effort = [0.0, 0.0, 0.0, 0.0, 0.0]

        #rospy.loginfo('Desired State :' +str(q_desired[3]))
        #rospy.loginfo('Current State :' +str(self.current_state[3]))
        #rospy.loginfo('Comand ' + str(q_cmd[3]))
        #desJoints.position[3] = -1*q_cmd[3]
        #rospy.loginfo('********************************')
        self.pub_joint_command.publish(desJoints)

        #Check if the arm is in the correct position
        #Only used when a service is controlling
        if self.srv_control:
            #rospy.loginfo('Difference ' + str(np.abs(self.current_state - q_desired)))
            if all(np.abs(self.current_state[0:3] - q_desired[0:3]) < 0.2):
                self.srv_control = False

    def directControl(self, q_desired):
        q_cmd = q_desired*self.maxVoltatge
        desJoints = JointState()
        desJoints.header.stamp = rospy.get_rostime()
        desJoints.header.frame_id = ""

        desJoints.position = q_cmd
        #desJoints.position = [0, q1cmd,  0,  0,  0]
        desJoints.effort = [0.0, 0.0, 0.0, 0.0, 0.0]

        #rospy.loginfo('Comand ' + str(q_cmd[3]))
        self.pub_joint_command.publish(desJoints)

    def velControl(self, vel_desired):
        self.lock_comand_vel.acquire()
        #rospy.loginfo('********* Vel Control ***********')
        #rospy.loginfo('Vel Desired ' + str(vel_desired[3]))
        try:
            if self.new_roll_vel:
                qdot_des = (self.kq[3]*(self.current_state[3] - vel_desired[3]) +
                            self.kdq[3]*(self.current_state[3] - self.previous_state[3])
                            / self.dt)
                if qdot_des > self.maxSaturate[3]:
                    qdot_des = self.maxSaturate[3]
                elif qdot_des < (-1*self.maxSaturate[3]):
                    qdot_des = -1*self.maxSaturate[3]
                #Convert to the voltatge range
                vroll_desired = int(qdot_des*self.maxVoltatge)

                #rospy.loginfo('Qdot_des ' + str(qdot_des) + ' vroll ' + str(vroll_desired))

                if abs(self.current_state[3] - vel_desired[3]) < 0.02:
                    #rospy.loginfo('To small Value in the Roll')
                    vroll_desired = 0
                if vroll_desired < self.minPosVolRoll and vroll_desired > 0:
                    #rospy.loginfo('To Minimum Value Pos')
                    vroll_desired = self.minPosVolRoll
                elif vroll_desired > self.minNegVolRoll and vroll_desired < 0:
                    #rospy.loginfo('To Minimum Value Neg')
                    vroll_desired = self.minNegVolRoll
                self.new_roll_vel = False
            else:
                vroll_desired = 0.0

            desJoints = JointState()
            desJoints.header.stamp = rospy.get_rostime()
            desJoints.header.frame_id = ""
            desJoints.position = np.copy(vel_desired)
            #rospy.loginfo('Command to the ROLL ' + str(vroll_desired))
            #desJoints.position[3] = vroll_desired
            desJoints.position[3] = vroll_desired
            #rospy.loginfo('Command After ' + str(desJoints.position[3]))
            #desJoints.position = [0, q1cmd,  0,  0,  0]
            desJoints.effort = [0.0, 0.0, 0.0, 0.0, 0.0]

            #rospy.loginfo('Comand ' + str(vel_desired))
            self.pub_joint_command.publish(desJoints)
        finally:
            self.lock_comand_vel.release()

    def publish_fk(self):
        ori = tf.transformations.quaternion_from_euler(self.base_ori[0],
                                                       self.base_ori[1],
                                                       self.base_ori[2])
        self.br.sendTransform(
            (self.base_pose[0], self.base_pose[1], self.base_pose[2]),
            (ori[0], ori[1], ori[2], ori[3]),
            rospy.Time.now(),
            'base_arm',
            'girona500')

        jointAngles = JntArray(self.numDoF-1)
        for i in xrange(self.numDoF-1):
            jointAngles[i] = self.current_state[i]
        ef_frame = Frame()
        if (self.fk_solver.JntToCart(jointAngles, ef_frame) >= 0):
            #Forward kinematic compute correctly
            pose = PoseStamped()
            pose.header.stamp = rospy.get_rostime()
            pose.header.frame_id = "base_arm"

            pose.pose.position.x = ef_frame.p[0]
            pose.pose.position.y = ef_frame.p[1]
            pose.pose.position.z = ef_frame.p[2]

            current_time = rospy.get_time()
            vel_end_effector = np.zeros(3)
            vel_end_effector[0] = (ef_frame.p[0] - self.current_pose_ef[0]) / (current_time - self.time)
            vel_end_effector[1] = (ef_frame.p[1] - self.current_pose_ef[1]) / (current_time - self.time)
            vel_end_effector[2] = (ef_frame.p[2] - self.current_pose_ef[2]) / (current_time - self.time)
            speed_end_effector = np.sqrt( vel_end_effector[0]**2 + vel_end_effector[1]**2 + vel_end_effector[2]**2 )

            velocity_msg = VelocityAndSpeed()
            velocity_msg.header.stamp = rospy.get_rostime()
            velocity_msg.velocity = vel_end_effector
            velocity_msg.speed = speed_end_effector
            self.pub_vel_speed_ee.publish(velocity_msg)

            self.current_pose_ef[0] = ef_frame.p[0]
            self.current_pose_ef[1] = ef_frame.p[1]
            self.current_pose_ef[2] = ef_frame.p[2]

            self.time = current_time

            quat = ef_frame.M.GetQuaternion()
            pose.pose.orientation.x = quat[0]
            pose.pose.orientation.y = quat[1]
            pose.pose.orientation.z = quat[2]
            pose.pose.orientation.w = quat[3]

            # euler = tf.transformations.euler_from_quaternion(
            #     [quat[0], quat[1], quat[2], quat[3]])
            rpy = ef_frame.M.GetRPY()
            self.current_pose_ef[3] = rpy[0]
            self.current_pose_ef[4] = rpy[1]
            self.current_pose_ef[5] = rpy[2]

            # rospy.loginfo('Roll recived ' + str(self.current_state[3]))
            # rospy.loginfo('RPY Solved ' + str(rpy[0]) + ', ' +
            #               str(rpy[1]) + ', ' + str(rpy[2]))
            quat2 = tf.transformations.quaternion_from_euler(
                rpy[0], rpy[1], self.current_state[3])

            #rospy.loginfo('Publishig RPY ' + str(self.current_pose_ef[3]) + str(self.current_pose_ef[4]) +str(self.current_pose_ef[5]))

            #initialize the first orientation
            if (not self.init_ori_hand and
                np.any(rpy != np.zeros(3))):
                self.initial_ori[0] = rpy[0]
                self.initial_ori[1] = rpy[1]
                self.initial_ori[2] = rpy[2]
                self.init_ori_hand = True

            self.pub_fk.publish(pose)
            self.br.sendTransform(
                (ef_frame.p[0], ef_frame.p[1], ef_frame.p[2]),
                (quat[0], quat[1], quat[2], quat[3]),
                rospy.Time.now(),
                'end_effector',
                'base_arm')

            self.br.sendTransform(
                (ef_frame.p[0], ef_frame.p[1], ef_frame.p[2]),
                (quat2[0], quat2[1], quat2[2], quat2[3]),
                rospy.Time.now(),
                'end_effector_v2',
                'base_arm')

            # The orientation of the camera in the simulator is displaced fr
            # r_m_ee = tf.transformations.quaternion_matrix(quat)
            # r_m = tf.transformations.euler_matrix(0, 0, 1.57)
            # rot = np.dot(r_m_ee, r_m)
            # #Send camera tf
            # self.br.sendTransform(
            #     (ef_frame.p[0], ef_frame.p[1]-0.02, ef_frame.p[2]),
            #     (tf.transformations.quaternion_from_matrix(rot)),
            #     rospy.Time.now(),
            #     'g500/hand_camera',
            #     'base_arm')

            camera_rot_quat = tf.transformations.quaternion_from_euler(0, 0, 0)
            self.br.sendTransform(
                (0.0,  0.0, -3.0),
                camera_rot_quat,
                rospy.Time.now(),
                'g500/check_tf',
                'girona500')

        else:
            rospy.logerr('Error in the Forward Kinematics')

        # if (self.fk_solver.JntToCart(jointAngles, ef_frame, 1) >= 0):

        #     quat = ef_frame.M.GetQuaternion()

        #     self.br.sendTransform(
        #         (ef_frame.p[0], ef_frame.p[1], ef_frame.p[2]),
        #         (quat[0], quat[1], quat[2], quat[3]),
        #         rospy.Time.now(),
        #         'slew',
        #         'base_arm')
        # else:
        #     rospy.logerr('Error computing the Slew TF')

        if (self.fk_solver.JntToCart(jointAngles, ef_frame, 1) >= 0):

            quat = ef_frame.M.GetQuaternion()

            self.br.sendTransform(
                (ef_frame.p[0], ef_frame.p[1], ef_frame.p[2]),
                (quat[0], quat[1], quat[2], quat[3]),
                rospy.Time.now(),
                'slew',
                'base_arm')
        else:
            rospy.logerr('Error computing the Slew TF')

        if (self.fk_solver.JntToCart(jointAngles, ef_frame, 2) >= 0):

            quat = ef_frame.M.GetQuaternion()

            self.br.sendTransform(
                (ef_frame.p[0], ef_frame.p[1], ef_frame.p[2]),
                (quat[0], quat[1], quat[2], quat[3]),
                rospy.Time.now(),
                'elevation',
                'base_arm')
        else:
            rospy.logerr('Error computing the Elevation TF')

        if (self.fk_solver.JntToCart(jointAngles, ef_frame, 3) >= 0):

            quat = ef_frame.M.GetQuaternion()

            self.br.sendTransform(
                (ef_frame.p[0], ef_frame.p[1], ef_frame.p[2]),
                (quat[0], quat[1], quat[2], quat[3]),
                rospy.Time.now(),
                'elbow',
                'base_arm')
        else:
            rospy.logerr('Error computing the Elbow TF')

        #Send camera tf
        try:
            trans, rot = self.tflistener.lookupTransform(
                "elbow", "end_effector",
                self.tflistener.getLatestCommonTime(
                    "elbow", "end_effector"))
            # t_aux = np.asarray(trans)
            # t_aux = t_aux + self.offset_ee_camera[0:3]
            t_aux = self.current_pose_ef[0:3] + self.offset_ee_camera[0:3]
            #t_aux[1] += 0.02
            #t_aux[2] += 0.05
            #r_m_ee = tf.transformations.quaternion_matrix(quat)
            r_hand = tf.transformations.euler_matrix(self.offset_ee_camera[3],
                                                     self.offset_ee_camera[4],
                                                     self.offset_ee_camera[5])
            #ori = tf.transformations.euler_matrix(self.initial_ori[0],
            #                                      self.initial_ori[1],
            #                                      self.initial_ori[2])
            curr_ori = tf.transformations.euler_matrix(self.current_pose_ef[3],
                                                       self.current_pose_ef[4],
                                                       self.current_pose_ef[5])
            #ori_inv = tf.transformations.inverse_matrix(ori)
            #dif = np.dot(ori_inv, curr_ori)
            hand_ori = np.dot(curr_ori, r_hand)
            #r_aux = np.dot(r_m_ee, r_m)
            # self.br.sendTransform(
            #     t_aux,
            #     tf.transformations.quaternion_from_matrix(hand_ori),
            #     rospy.Time.now(),
            #     'g500/hand_camera',
            #     'elbow')
            self.br.sendTransform(
                t_aux,
                tf.transformations.quaternion_from_matrix(hand_ori),
                rospy.Time.now(),
                'camera',
                'base_arm')
        except tf.Exception:
            rospy.logerr(
                'Error reading the Tranformation from elbow to EE')


    def calcJacobian(self):
        #We aboid the gripper
        jointAngles = JntArray(self.numDoF-1)
        for i in xrange(self.numDoF-1):
            jointAngles[i] = self.current_state[i]

        jac = Jacobian(4)
        if self.jac_solver.JntToJac(jointAngles, jac) >= 0:
            #Take only the 3x3 part of the 6x3 Jacobian
            self.useJacobian[0, 0] = jac[0, 0]
            self.useJacobian[0, 1] = jac[0, 1]
            self.useJacobian[0, 2] = jac[0, 2]
            self.useJacobian[1, 0] = jac[1, 0]
            self.useJacobian[1, 1] = jac[1, 1]
            self.useJacobian[1, 2] = jac[1, 2]
            self.useJacobian[2, 0] = jac[2, 0]
            self.useJacobian[2, 1] = jac[2, 1]
            self.useJacobian[2, 2] = jac[2, 2]
        else:
            rospy.logerr('Error in the Forward Kinematics')

    #This function compute the IK for a desired angles and return a
    # 4 DoF vector for the csip_e5_arm
    #If There is an error retuns:
    # -1 first joint limit reached
    # -2 second -3 third.
    def computeInverseKinematics(self, desired_pose):

        desired_joints = np.zeros([self.numDoF])
        error = 0

        x = desired_pose[0]
        y = desired_pose[1]
        z = desired_pose[2]

        # Theta 0, represents the soulder
        desired_joints[0] = np.arctan2(y, x)
        #rospy.loginfo('Theta 0 = ' + str(desired_joints[0]))
        if ((desired_joints[0] > self.max_joint[0]) or (desired_joints[0]
                                                        < self.min_joint[0])):
            error = -1
        # d = Distance from the base of the arm to the desired pose in the 2D
        # X,Y of the base.
        d = np.sqrt((x**2)+(y**2))
        #rospy.loginfo('D = ' + str(d))

        # t = Distance from the joint1 (elevation) to the desired pose in the
        # 2D x,z of the base.
        t = np.sqrt((z**2)+((d-self.DH_a1)**2))
        #rospy.loginfo('t = ' + str(t))

        # beta = angle computed to use the cos theorem. Is the angle between
        # the a2 and l. Doing a Triangle with t (a2,l,t)
        beta = np.arccos(((t**2) - (self.DH_a2**2) - (self.IK_l**2)) /
                         (-2*self.DH_a2*self.IK_l))
        #rospy.loginfo('beta = ' + str(beta))
        # theta 2, represents the elbow
        desired_joints[2] = (-1*np.pi) + beta + self.IK_alpha
        #rospy.loginfo('theta 2 = ' + str(desired_joints[2]))
        if (np.isnan(desired_joints[2])):
            desired_joints[2] = np.pi
            error = -3
        if ((desired_joints[2] > self.max_joint[2]) or (desired_joints[2]
                                                        < self.min_joint[2])):
            error = -3
        # phi = is the angle between the a2(elevation) and the end of the
        # joint3 (forearm).
        phi = np.arctan2(self.IK_l*np.sin(self.IK_alpha-desired_joints[2]),
                         self.DH_a2 + (self.IK_l*np.cos(
                             self.IK_alpha - desired_joints[2])))
        #rospy.loginfo('phi = ' + str(phi))
        #gamma = angle from the Joint1 (elevation) to (fore arm)
        gamma = np.arctan2(-1*z, (d - self.DH_a1))

        #rospy.loginfo('Gamma ' + str(gamma) + ' phi ' + str(phi))

        # theta 1, represents the elevation
        desired_joints[1] = gamma + phi
        #rospy.loginfo('theta 1 = ' + str(desired_joints[1]))
        if ((desired_joints[1] > self.max_joint[1]) or (desired_joints[1]
                                                        < self.min_joint[1])):
            error = -2

        #theta 3, represents directly the roll of the arm
        desired_joints[3] = desired_pose[3]

        if error == 0:
            return desired_joints
        else:
            desired_joints[:] = error
            return desired_joints

    def turn_90_degres(self, req):
        if not self.srv_control:
            self.lock_comand_pose.acquire()
            try:
                self.srv_control = False
                self.volt_control = False
                self.jnt_control = False
                self.pose_control = True
                self.vel_control = False
                self.des_q_pose[0] = self.current_state[0]
                self.des_q_pose[1] = self.current_state[1]
                self.des_q_pose[2] = self.current_state[2]
                self.des_q_pose[3] = cola2_lib.wrapAngle(
                    self.current_state[3] + np.deg2rad(90))
                rospy.loginfo('Desired Movement ' + str(self.des_q_pose[3]) +
                              ' Current State ' + str(self.current_state[3]))
                self.des_q_pose[4] = self.current_state[4]
                # self.des_q_vel[0] = 0.0 #self.current_state[0]
                # self.des_q_vel[1] = 0.0 #self.current_state[1]
                # self.des_q_vel[2] = 0.0 #self.current_state[2]
                # self.pose_control = False
                # self.vel_control = True
                # self.des_q_vel[0] = 0.0 #self.current_state[0]
                # self.des_q_vel[1] = 0.0 #self.current_state[1]
                # self.des_q_vel[2] = 0.0 #self.current_state[2]
                # self.des_q_vel[3] = cola2_lib.wrapAngle(
                #     self.current_state[3] + np.deg2rad(90))
                # rospy.loginfo('Desired Movement ' + str(self.des_q_vel[3]) +
                #               ' Current State ' + str(self.current_state[3]))
                # self.des_q_vel[4] = 0
            finally:
                self.lock_comand_pose.release()
        return EmptyResponse()

    def turn_desired_degrees(self, req):
        """
        Turn the desired amount of degrees sended in the message, this service
        return false if the angle is not possible and true if has been done
        successfully
        """
        if not self.srv_control:
            self.lock_comand_pose.acquire()
            try:
                self.srv_control = False
                self.volt_control = False
                self.jnt_control = False
                self.pose_control = False
                self.vel_control = False
                self.des_q_pose[0] = self.current_state[0]
                self.des_q_pose[1] = self.current_state[1]
                self.des_q_pose[2] = self.current_state[2]
                self.des_q_pose[3] = cola2_lib.wrapAngle(
                    self.current_state[3] + np.deg2rad(req.desired_degree))
                rospy.loginfo('Desired Movement ' + str(self.des_q_pose[3]) +
                              ' Current State ' + str(self.current_state[3]))
                self.des_q_pose[4] = self.current_state[4]
                # self.des_q_vel[0] = 0.0 #self.current_state[0]
                # self.des_q_vel[1] = 0.0 #self.current_state[1]
                # self.des_q_vel[2] = 0.0 #self.current_state[2]
                # self.pose_control = False
                # self.vel_control = True
                # self.des_q_vel[0] = 0.0 #self.current_state[0]
                # self.des_q_vel[1] = 0.0 #self.current_state[1]
                # self.des_q_vel[2] = 0.0 #self.current_state[2]
                # self.des_q_vel[3] = cola2_lib.wrapAngle(
                #     self.current_state[3] + np.deg2rad(90))
                # rospy.loginfo('Desired Movement ' + str(self.des_q_vel[3]) +
                #               ' Current State ' + str(self.current_state[3]))
                # self.des_q_vel[4] = 0
                watch_dog = 0
                while (np.abs(self.current_state[3] - self.des_q_pose[3]) > 0.05 
                       and not rospy.is_shutdown()
                       and watch_dog <= 50):
                    self.control(self.des_q_pose)
                    watch_dog = watch_dog+1
                    rospy.sleep(self.period)
                resp = TurnDesiredDegreesResponse()
                resp.success = True
                return resp
            finally:
                self.lock_comand_pose.release()
        return EmptyResponse()

    def turn_desired_radians(self, req):
        """
        Turn the desired amount of degrees sended in the message, this service
        return false if the angle is not possible and true if has been done
        successfully
        """
        if not self.srv_control:
            self.lock_comand_pose.acquire()
            try:
                self.srv_control = False
                self.volt_control = False
                self.jnt_control = False
                self.pose_control = False
                self.vel_control = False
                self.des_q_pose[0] = self.current_state[0]
                self.des_q_pose[1] = self.current_state[1]
                self.des_q_pose[2] = self.current_state[2]
                self.des_q_pose[3] = cola2_lib.wrapAngle(
                    self.current_state[3] + req.desired_degree)
                rospy.loginfo('Desired Movement ' + str(self.des_q_pose[3]) +
                              ' Current State ' + str(self.current_state[3]))
                self.des_q_pose[4] = self.current_state[4]
                # self.des_q_vel[0] = 0.0 #self.current_state[0]
                # self.des_q_vel[1] = 0.0 #self.current_state[1]
                # self.des_q_vel[2] = 0.0 #self.current_state[2]
                # self.pose_control = False
                # self.vel_control = True
                # self.des_q_vel[0] = 0.0 #self.current_state[0]
                # self.des_q_vel[1] = 0.0 #self.current_state[1]
                # self.des_q_vel[2] = 0.0 #self.current_state[2]
                # self.des_q_vel[3] = cola2_lib.wrapAngle(
                #     self.current_state[3] + np.deg2rad(90))
                # rospy.loginfo('Desired Movement ' + str(self.des_q_vel[3]) +
                #               ' Current State ' + str(self.current_state[3]))
                # self.des_q_vel[4] = 0
                watch_dog = 0
                while (np.abs(self.current_state[3] - self.des_q_pose[3]) > 0.05
                       and not rospy.is_shutdown()
                       and watch_dog <= 50):
                    self.control(self.des_q_pose)
                    watch_dog = watch_dog + 1
                    rospy.sleep(self.period)
                # Work Around ( some time it keep small command )
                # Publish 0 velocity to all the joints to stop the movement.
                desJoints = JointState()
                desJoints.header.stamp = rospy.get_rostime()
                desJoints.header.frame_id = ""
                desJoints.position = [0.0, 0.0, 0.0, 0.0, 0.0]
                desJoints.effort = [0.0, 0.0, 0.0, 0.0, 0.0]
                self.pub_joint_command.publish(desJoints)

                resp = TurnDesiredDegreesResponse()
                if watch_dog <= 50:
                    resp.success = True
                else:
                    resp.success = False
                return resp
            finally:
                self.lock_comand_pose.release()
        return EmptyResponse()

    def run(self):
        while not rospy.is_shutdown():
            if self.initialize:
                self.publish_fk()
                self.calcJacobian()
                if self.volt_control:
                    self.directControl(self.des_q_vlt)
                elif self.jnt_control:
                    self.control(self.des_q_jnt)
                elif self.pose_control:
                    self.control(self.des_q_pose)
                elif self.vel_control:
                    self.velControl(self.des_q_vel)
            #move if is required
            rospy.sleep(self.period)

if __name__ == '__main__':
    try:
        rospy.init_node('arm_controller')
        arm_controller = armControllerDev(rospy.get_name())
        arm_controller.run()
        #csip_e5_arm.test_pid()
    except rospy.ROSInterruptException:
        rospy.logerr('The arm_controller has stopped unexpectedly')
