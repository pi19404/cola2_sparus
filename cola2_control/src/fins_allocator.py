#!/usr/bin/env python

# ROS imports
import roslib
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from cola2_control.msg import Setpoints
from auv_msgs.msg import NavSts, BodyForceReq
from std_srvs.srv import Empty, EmptyResponse

# Python imports
from numpy import array, matrix, clip, asarray, zeros, squeeze, size

# Custom imports
from cola2_lib import cola2_ros_lib

class FinsAllocator:
    """ Translate from forces to fins setpoints """

    def __init__(self, name):
        """ Constructor  """
        self.name = name

        # Enable parameter
        self.enable = True

        # Default parameters
        self.n_fins = 1                             # number of fins
        self.fpl = [0, 1]                           # linear fins model
        self.force_to_fins_ratio = 30.0             # ratio

        # Load parameters
        self.get_config()

        # Initialize navigation parameters
        self.vel = array( [0.0, 0.0, 0.0] )

        # Create publisher
        self.pub_fins = rospy.Publisher("/cola2_control/fins_data", Setpoints)

        # Create Subscriber
        rospy.Subscriber("/cola2_control/merged_body_force_req", BodyForceReq, self.main_callback)
        rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.update_nav_sts)

        # Create services
        self.e_srv = rospy.Service('/cola2_control/enable_fins_allocator', Empty, self.enable_srv)
        self.d_srv = rospy.Service('/cola2_control/disable_fins_allocator', Empty, self.disable_srv)

        # Show message
        rospy.loginfo("%s: initialized", self.name)


    def main_callback(self, data) :
        """ Callback and main function """
        if ( self.enable ) :
            # Input data
            body_force_req = zeros(6)

            if not data.disable_axis.x :
                body_force_req[0] = data.wrench.force.x
            if not data.disable_axis.y :
                body_force_req[1] = data.wrench.force.y
            if not data.disable_axis.z :
                body_force_req[2] = data.wrench.force.z
            if not data.disable_axis.roll :
                body_force_req[3] = data.wrench.torque.x
            if not data.disable_axis.pitch :
                body_force_req[4] = data.wrench.torque.y
            if not data.disable_axis.yaw :
                body_force_req[5] = data.wrench.torque.z

            rospy.loginfo("%s: body force request: %s", self.name, str(body_force_req))

            # Computes the force to be done for each actuator
            force = self.fcm_inv * matrix(body_force_req).T
            force = squeeze(asarray(force)).ravel() # Matrix to 1D array
            rospy.loginfo("%s: fins raw forces: %s", self.name, str(force))

            # Linearize actuator forces
            force = self.polynomial_model_to_actuators_force(force)
            rospy.loginfo("%s: fins forces: %s", self.name, str(force))

            # Adjust Newtons to angles
            angles = self.force_to_angles(force)
            rospy.loginfo("%s: fins angles: %s", self.name, str(angles))

            # Adjust angles to setpoints (sature)
            setpoints = self.angles_to_setpoints(angles)
            rospy.loginfo("%s: fins setpoint: %s", self.name, str(setpoints))

            # Publish computed data
            fins = Setpoints()
            fins.header.stamp = rospy.Time.now()
            fins.header.frame_id = self.frame_id
            fins.setpoints = list( array( setpoints ).ravel() )
            self.pub_fins.publish(fins)


    def polynomial_model_to_actuators_force(self, data) :
        """ Compute polynomial correction to fins forces """
        ret = zeros( len(data) ) # I use size() to avoid len() of a number

        for fn in range( len(data) ) :
            if data[fn] < 0.0 :
                fn_value = abs(data[fn])
                change_sign = True
            else :
                fn_value = data[fn]
                change_sign = False

            for it in range( len(self.fpl) ) :
                ret[fn] = ret[fn] + pow( fn_value, self.fple[it] ) * self.fpl[it]

            if change_sign:
                ret[fn] = ret[fn] * (-1.0)

        return ret


    def force_to_angles(self, data) :
        """ Compute newtons to angle for each fin """
        # TODO: compute models for each actuator instead of using simple ratios
        return data / self.force_to_fins_ratio


    def angles_to_setpoints(self, data) :
        """ Saturate fins angle and convert them to -1 to 1 """
        if self.max_angle != 0.0 :
            data = clip(asarray(data)/float(abs(self.max_angle)), -1, 1)
        else :
            data = zeros(size(data))
        return data


    def update_nav_sts(self, nav_sts):
        """ Update vehicle velocity. It will be needed to compute fins models """
        self.vel[0] = nav_sts.body_velocity.x
        self.vel[1] = nav_sts.body_velocity.y
        self.vel[2] = nav_sts.body_velocity.z


    def enable_srv(self, req):
        """ Enable fins service """
        self.enable = True
        rospy.loginfo('%s: enabled', self.name)
        return EmptyResponse()


    def disable_srv(self, req):
        """ Disable fins service """
        self.enable = False
        rospy.loginfo('%s: disabled', self.name)
        return EmptyResponse()


    def get_config(self) :
        """ Get config from the rosparam server """
        param_dict = {'frame_id': 'fins_allocator/frame_id',
                      'n_fins': 'fins_allocator/n_fins',
                      'fpl': 'fins_allocator/fins_polynomial_linearization',
                      'fple': 'fins_allocator/fins_polynomial_linearization_exp',
                      'force_to_fins_ratio': 'fins_allocator/force_to_fins_ratio',
                      'max_angle': 'fins_allocator/max_angle',
                      'fcm': 'fins_allocator/fins_control_matrix'}

        if not cola2_ros_lib.getRosParams(self, param_dict, self.name):
            rospy.logfatal("%s: shutdown due to invalid config parameters!", self.name)
            exit(0)  # TODO: find a better way

        # Resize...
        self.fcm = list(array(self.fcm).ravel())
        self.fcm = matrix(array(self.fcm).reshape(6, size(self.fcm) / 6))
        self.fcm_inv = self.fcm.I


if __name__ == '__main__':
    try:
        rospy.init_node( 'fins_allocator' )
        fins_allocator = FinsAllocator( rospy.get_name() )
        rospy.spin()
    except rospy.ROSInterruptException: pass
