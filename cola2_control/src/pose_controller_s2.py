#!/usr/bin/env python
"""@@Position controller of Sparus II AUV.@@"""

# ROS imports
import roslib
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from auv_msgs.msg import BodyVelocityReq
from auv_msgs.msg import WorldWaypointReq
from auv_msgs.msg import NavSts
from auv_msgs.msg import GoalDescriptor

# Python imports
import numpy as np

# Custom imports
from cola2_lib import cola2_lib, cola2_ros_lib


class PoseControllerS2 :
    def __init__(self, name):
        """ Controls the velocity and pose of an AUV  """
        self.name = name

        # Load parameters
        self.get_config()
        self.goal = GoalDescriptor()

        # Init input data
        self.pose = np.zeros(6)
        self.v = np.zeros(6)
        self.desired_pose = np.zeros(6)
        self.req = WorldWaypointReq()
        self.altitude = 1.0

        # Check navigator
        self.init_navigator_check = False
        self.navigator_ok = False
        rospy.Timer(rospy.Duration(1.0), self.navigator_check)

        # Create publisher
        self.pub_tau = rospy.Publisher("/cola2_control/body_velocity_req", BodyVelocityReq)

        # Create Subscriber
        rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.update_nav_sts)
        rospy.Subscriber("/cola2_control/merged_world_waypoint_req", WorldWaypointReq, self.compute_request)

        # Show message
        rospy.loginfo("%s: initialized", self.name)


    def update_nav_sts(self, nav_sts):
        """ Store position and velocity from navigation data  """
        self.pose[2] = nav_sts.position.depth
        self.pose[3] = nav_sts.orientation.roll
        self.pose[4] = nav_sts.orientation.pitch
        self.pose[5] = nav_sts.orientation.yaw
        self.altitude = nav_sts.altitude
        self.v[0] = nav_sts.body_velocity.x
        self.v[1] = nav_sts.body_velocity.y
        self.v[2] = nav_sts.body_velocity.z
        self.v[3] = nav_sts.orientation_rate.roll
        self.v[4] = nav_sts.orientation_rate.pitch
        self.v[5] = nav_sts.orientation_rate.yaw

        self.last_navigator_callback = rospy.Time.now()
        if not self.init_navigator_check:
            self.init_navigator_check = True


    def navigator_check(self, event):
        """ Timer to check navigator """
        if self.init_navigator_check:
            if (rospy.Time.now() - self.last_navigator_callback).to_sec() < 2.0:
                self.navigator_ok = True
            else:
                self.navigator_ok = False


    def compute_request(self, req):
        """ Callback: computes pose request """
        if self.navigator_ok:
            # Store data
            self.goal = req.goal

            # Depth
            if req.altitude_mode:
                self.desired_pose[2] = self.pose[2] + self.altitude - req.altitude
            else:
                self.desired_pose[2] = req.position.depth

            # Pitch
            self.desired_pose[4] = cola2_lib.saturateValueFloat(req.orientation.pitch, self.max_pitch)

            # Yaw
            self.desired_pose[5] = req.orientation.yaw
            self.req = req
            if not req.disable_axis.x or not req.disable_axis.y:
                rospy.logerr("%s: pose_controller can't control SURGE or SWAY DoFs", self.name)


            # Main loop
            #rospy.loginfo("%s: desired pose: %s", self.name, str(self.desired_pose))
            #rospy.loginfo("%s: current pose: %s", self.name, str(self.pose))

            # Compute error
            error = self.desired_pose - self.pose
            error[3] = cola2_lib.wrapAngle(error[3])
            error[4] = cola2_lib.wrapAngle(error[4])
            error[5] = cola2_lib.wrapAngle(error[5])

            # Compute all TAU using a PID
            pid_tau  = self.pid.compute(error, np.zeros(6), rospy.Time.now().to_sec())

            # Compute depth with fins PID
            depth_with_fins = False
            #if ( self.v[0] > self.start_depth_with_fins_velocity and self.pose[2] > self.surface_zone ) :
            #    if ( self.req.disable_axis.pitch == True ) :  # Depth with fins
            #        pid_tau[4] = self.depth_with_fins_pid.compute(np.array([self.desired_pose[2] - self.pose[2]]), np.array([0.0]), rospy.Time.now().to_sec())
            #        depth_with_fins = True
            #        self.pid.resetDof(4)
            #    else :  # User controlled pitch
            #        self.depth_with_fins_pid.eik_1 = 0.0  # Set to zero integral error of depth with fins PID
            #
            #    if ( self.v[0] > self.start_depth_with_fins_velocity + 0.10 ) :  # If fins are enabled and velocity is even higher, stop vertical thruster
            #        pid_tau[2] = 0.0
            #        self.pid.resetDof(2)
            #        rospy.loginfo('%s: depth control off!', self.name)
            #
            #else :  # No pitch
            #    pid_tau[4] = 0.0
            pid_tau[4] = 0.0

            #rospy.loginfo('%s: pid_tau: %s', self.name, pid_tau)

            # From pid_tau to velocity
            desired_velocity = cola2_lib.saturateValue(pid_tau, 1.0) * self.velocity_max

            # From depth velocity to surge and heave velocity (merge both)
            depth_velocity = desired_velocity[2]
            desired_velocity[2] = np.cos( self.pose[4] ) * depth_velocity
            desired_velocity[0] = desired_velocity[0] - np.sin( self.pose[4] ) * depth_velocity

            # Use fins when AUV is nearly stopped to drive vehicle pitch to zero
            fins_when_slow = False
            if False:
                if ( abs( self.v[0] ) < 0.2 ) :
                    helper_angle = 0.1
                    if ( self.v[0] > 0.05 ) :  # If small positive surge
                        fins_when_slow = True
                        if ( self.pose[4] > 0.0 ) :  # TODO: check thrusters
                            desired_velocity[4] = -helper_angle
                        else :
                            desired_velocity[4] = helper_angle
                    if ( self.v[0] < -0.05 ) :  # If small negative surge
                        fins_when_slow = True
                        if ( self.pose[4] > 0.0 ) :
                            desired_velocity[4] = helper_angle
                        else :
                            desired_velocity[4] = -helper_angle

            # Fill the BodyVelocityReq message
            data = BodyVelocityReq()
            data.goal = self.goal
            data.goal.requester = self.name
            data.header.stamp = rospy.Time.now()
            data.header.frame_id = "sparus2"

            # SURGE
            data.disable_axis.x = True
            data.twist.linear.x = 0.0

            # SWAY
            data.disable_axis.y = True
            data.twist.linear.y = 0.0

            # HEAVE
            data.disable_axis.z = self.req.disable_axis.z
            if data.disable_axis.z:
                data.twist.linear.z = 0.0
                self.pid.resetDof(2)
            else:
                data.twist.linear.z = desired_velocity[2]

            # ROLL
            data.disable_axis.roll = True
            data.twist.angular.x = 0.0

            # PITCH
            #data.disable_axis.pitch = not ((not self.req.disable_axis.pitch) or depth_with_fins or fins_when_slow)
            data.disable_axis.pitch = True
            if data.disable_axis.pitch:
                data.twist.angular.y = 0.0
                self.pid.resetDof(4)
            else:
                data.twist.angular.y = desired_velocity[4]

            # YAW
            data.disable_axis.yaw = self.req.disable_axis.yaw
            if data.disable_axis.yaw:
                data.twist.angular.z = 0.0
                self.pid.resetDof(5)
            else:
                data.twist.angular.z = desired_velocity[5]

            # Publish data
            #rospy.loginfo("%s: tau: %s", self.name, data.twist)
            self.pub_tau.publish(data)

        else:
            self.pid.reset(rospy.Time.now().to_sec())
            self.depth_with_fins_pid.reset(rospy.Time.now().to_sec())


    def get_config(self) :
        """ Load parameters from the rosparam server """
        param_dict = {'velocity_max': 'pose_controller/velocity_max',
                      'start_depth_with_fins_velocity': 'pose_controller/start_depth_with_fins_velocity',
                      'max_pitch': 'pose_controller/max_pitch',
                      'surface_zone': 'pose_controller/surface_zone',
                      'ffv': 'pose_controller/pid_pose_feed_forward_velocity',
                      'kp': 'pose_controller/pid_pose_kp',
                      'ti': 'pose_controller/pid_pose_ti',
                      'td': 'pose_controller/pid_pose_td',
                      'i_limit': 'pose_controller/pid_pose_i_limit',
                      'pid_depth_with_fins_kp': 'pose_controller/pid_depth_with_fins_kp',
                      'pid_depth_with_fins_ti': 'pose_controller/pid_depth_with_fins_ti',
                      'pid_depth_with_fins_td': 'pose_controller/pid_depth_with_fins_td'}


        # PID Close loop parameters
        if not cola2_ros_lib.getRosParams(self, param_dict, self.name):
            rospy.logfatal("%s: shutdown due to invalid config parameters!", self.name)
            exit(0)  # TODO: find a better way

        # Resize...
        self.velocity_max = np.array(self.velocity_max)
        self.ffv = np.array(self.ffv)
        self.kp = np.array(self.kp)
        self.ti = np.array(self.ti)
        self.td = np.array(self.td)
        self.i_limit = np.array(self.i_limit)
        self.pid = cola2_lib.PID(self.kp, self.ti, self.td, self.ffv, rospy.Time.now().to_sec())
        self.depth_with_fins_pid = cola2_lib.PID([self.pid_depth_with_fins_kp], [self.pid_depth_with_fins_ti], [self.pid_depth_with_fins_td], [0.0], rospy.Time.now().to_sec())


if __name__ == '__main__':
    try:
        rospy.init_node('pose_controller_s2')
        __pose_controller__ = PoseControllerS2(rospy.get_name())
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
