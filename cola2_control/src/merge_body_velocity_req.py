#!/usr/bin/env python

"""
Created on 2013 updated on 31/03/2014
@author: Narcis Palomeras
"""

# ROS imports
import roslib
roslib.load_manifest('cola2_control')
import rospy

# Python imports
import copy

# Msgs imports
from auv_msgs.msg import BodyVelocityReq

# COLA2 imports
import merge
from cola2_lib import cola2_ros_lib


class MergeBodyVelocityReq:
    """@@This node is used to merge body_velocity_req messages, 
         taking into account message priorities and disabled axis.@@"""
 
    def __init__(self, name):
        """ Constructor for class MergeBodyVelocityReq."""
        self.name = name
        self.body_velocity_requests = list()
        self.frame_id = "/vehicle"

        # Get config
        param_dict = {'frame_id': 'merge/frame_id'}
        cola2_ros_lib.getRosParams(self, param_dict)
        
        # Initialize response        
        self.response = BodyVelocityReq()
        self.response.header.frame_id = self.frame_id
        self.last_response = copy.deepcopy(self.response)
        
        # Create publisher
        self.pub_coordinated = rospy.Publisher(
               "/cola2_control/merged_body_velocity_req", BodyVelocityReq)

        # Create subscriber
        rospy.Subscriber(
               "/cola2_control/body_velocity_req", BodyVelocityReq,
               self.update_response)


    def update_response(self, resp):
        """ Receive BodyVelocityReq messages and adds it in sp list"""
        
        # Add request
        merge.update_response(self.body_velocity_requests, resp)
        
        # Merge requests
        [value, disable_axis, goal] = merge.iterate(
                    self.body_velocity_requests, self.name)
        
        # Publish merged requests
        self.response.header.stamp = rospy.Time().now()
        self.response.twist.linear.x = value[0]
        self.response.twist.linear.y = value[1]
        self.response.twist.linear.z = value[2]
        self.response.twist.angular.x = value[3]
        self.response.twist.angular.y = value[4]
        self.response.twist.angular.z = value[5]
        self.response.disable_axis = disable_axis
        self.response.goal = goal
        
        if not merge.are_body_velocity_req_equal(self.response, 
                                                 self.last_response):
            self.pub_coordinated.publish(self.response)
            self.last_response = copy.deepcopy(self.response)
        
        
if __name__ == '__main__':
   
    try:
        rospy.init_node('merge_body_velocity_req')
        MBVR = MergeBodyVelocityReq(rospy.get_name())
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
