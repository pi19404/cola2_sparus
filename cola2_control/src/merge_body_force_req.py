#!/usr/bin/env python
""" Merge BodyForceReq messages """

"""
Created on 2013
@author: narcis palomeras
"""
"""@@This node is used to merge messages, taking into account message priorities, from
/cola2_control/body_force_req topic to /cola2_control/merged_body_force_req topic.@@"""

# ROS imports
import roslib
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from auv_msgs.msg import BodyForceReq

# Python imports
import merge

# More imports
from cola2_lib import cola2_ros_lib


# Global vars
sp = []


def update_response(resp):
    """ Receive BodyForceReq messages and adds it in sp list"""
    merge.update_response(sp, resp)


if __name__ == '__main__':
    """ Merge BodyForceReq messages """
    try:
        name = 'merge_body_force_req'
        rospy.init_node(name)

        # Get config
        config = cola2_ros_lib.Config()
        param_dict = {'rate': 'merge/rate',
                      'frame_id': 'merge/frame_id'}
        cola2_ros_lib.getRosParams(config, param_dict)

        # Create publisher
        pub_coordinated = rospy.Publisher(
               "/cola2_control/merged_body_force_req", BodyForceReq)

        # Create subscriber
        rospy.Subscriber(
               "/cola2_control/body_force_req", BodyForceReq,
               update_response)

        response = BodyForceReq()
        response.header.frame_id = config.frame_id

        r = rospy.Rate(config.rate)
        while not rospy.is_shutdown():
            [v, disable_axis, goal] = merge.iterate(sp, name)
            response.header.stamp = rospy.Time().now()
            response.wrench.force.x = v[0]
            response.wrench.force.y = v[1]
            response.wrench.force.z = v[2]
            response.wrench.torque.x = v[3]
            response.wrench.torque.y = v[4]
            response.wrench.torque.z = v[5]
            response.disable_axis = disable_axis
            response.goal = goal
            pub_coordinated.publish(response)
            r.sleep()
    except rospy.ROSInterruptException:
        pass
