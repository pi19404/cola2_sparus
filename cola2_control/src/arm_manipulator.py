#! /usr/bin/env python

import roslib; roslib.load_manifest('cola2_control')
import rospy

# Brings in the SimpleActionClient
import actionlib

# Brings in the messages used by the fibonacci action, including the
# goal message and the result message.
from cola2_control.msg import *

from cola2_control.srv import MoveArmTo

from geometry_msgs.msg import PoseStamped

#call the action and wait for the result of the action.
def send_arm_action(req):
    client = actionlib.SimpleActionClient('move_arm', cola2_control.msg.PoseArmReqAction)

    # Waits until the action server has started up and started
    # listening for goals.
    client.wait_for_server()

    # Creates a goal to send to the action server.
    goal = cola2_control.msg.PoseArmReqGoal( req.goal_pose )

    # Sends the goal to the action server.
    client.send_goal(goal)

    # Waits for the server to finish performing the action.
    client.wait_for_result()

    # Prints out the result of executing the action
    return client.get_result().pose_reached



if __name__ == '__main__':
    rospy.init_node('arm_manipulator')
    s = rospy.Service('move_arm_to', MoveArmTo, send_arm_action)
    print "Ready to move the arm"
    rospy.spin()

