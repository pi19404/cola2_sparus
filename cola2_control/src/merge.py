#!/usr/bin/env python
""" The functions in this file are used by all merge nodes """

"""
Created on 2013
@author: narcis palomeras
"""

# ROS imports
import roslib
roslib.load_manifest('cola2_control')
import rospy
import threading

# Msgs imports
from auv_msgs.msg import GoalDescriptor, Bool6Axis
from cola2_lib import cola2_lib


EXPIRE_TIME = 0.2       # Expire time of messages in sp
MINIMUM_PERIOD = 0.09   # Minimum period between 2 control signals

# This lock is required because some of the following
# functions can be executed simultaneously and some
# vars are shared
lock = threading.RLock()  

def update_response(sp, resp):
    """ Check if the requester has any previous request in sp.
        If there is one, remove it. This function is called by the
        specific merge in order to add a new request."""

    lock.acquire()

    # Delete previous request from the same requester
    for m in sp:
        if m.goal.requester == resp.goal.requester:
            sp.remove(m)

    # Add the new one
    sp.append(resp)

    lock.release()
    return sp


def all_axis_disabled(req):
    """ This function returns true if all DOFSs are disabled and false if 
        some of them are enabled. This is used by sort_responses """
    if not req.disable_axis.x:
        return False
    if not req.disable_axis.y:
        return False
    if not req.disable_axis.z:
        return False
    if not req.disable_axis.roll:
        return False
    if not req.disable_axis.pitch:
        return False
    if not req.disable_axis.yaw:
        return False

    return True


def sort_responses(sp):
    """ This function sorts sp and returns the sorted list. This is used by
        merge """
    # Create the empty response
    ret = []

    # Do a copy of the input
    sp_copy = list(sp)

    # while elements in the sp_copy vector
    while len(sp_copy) > 0:
        # Initialize max priority to a negative (not feasable) value
        max_priority = -1

        # Search the response with more priority
        for i in range(len(sp_copy)):
            if sp_copy[i].goal.priority > max_priority:
                max_priority = sp_copy[i].goal.priority
                index = i

        # Add the the response with highest priority and remove it from the
        # sp vector
        ret.append(sp_copy[index])
        sp_copy.remove(sp_copy[index])

    # If most priority response has all dofs disabled remove
    while len(ret) > 0 and all_axis_disabled(ret[0]):
        ret.remove(ret[0])

    # TODO: To be tested!
    # If there are several requests with maximum priority, combine them.
    end = False
    print 'SORT'
    while not end and len(ret) > 1:
        if ret[0].goal.priority == ret[1].goal.priority:
            print 'the firts 2 requests have the same priority'
            # print 'ret[0]: ', ret[0]
            # print 'ret[1]: ', ret[1]
            ret[0].disable_axis.x = (ret[0].disable_axis.x and ret[1].disable_axis.x)
            ret[0].disable_axis.y = (ret[0].disable_axis.y and ret[1].disable_axis.y)
            ret[0].disable_axis.z = (ret[0].disable_axis.z and ret[1].disable_axis.z)
            ret[0].disable_axis.roll = (ret[0].disable_axis.roll and ret[1].disable_axis.roll)
            ret[0].disable_axis.pitch = (ret[0].disable_axis.pitch and ret[1].disable_axis.pitch)
            ret[0].disable_axis.yaw = (ret[0].disable_axis.yaw and ret[1].disable_axis.yaw)
            if 'twist' in dir(ret[0]):
                print 'twist'
                v0 = get_twist(ret[0])
                v1 = get_twist(ret[1])
                set_twist(ret[0], v0, v1)
            elif 'wrench' in dir(ret[0]):
                print 'wrench'
                v0 = get_wrench(ret[0])
                v1 = get_wrench(ret[1])
                # print 'v0: ', v0
                # print 'v1: ', v1
                # print 'v0 + v1: ', list(np.array(v0) + np.array(v1))
                set_wrench(ret[0], v0, v1)
            elif 'altitude' in dir(ret[0]):
                print 'ERROR! Trying to merge two world waypoint requests with the same priority!!!'
                #v0 = get_pose(ret[0])
                #v1 = get_pose(ret[1])
                #set_pose(ret[0], list(np.array(v0) + np.array(v1)))
            else:
                rospy.logerr('[MERGE] Invalid type!' )
            # ret[0].goal.requester = ret[0].goal.requester + '__' +ret[1].goal.requester
            ret.remove(ret[1])
            print 'merged: ', ret[0]
        else:
            end = True
        print '\n\n'
    return ret


def match(req_axis, setted_axis):
    """ This function is used to check if setted responce and processed
        response are compatible. They are compatible when active processed
        response dofs are not active in the already setted response. This is
        used by merge """
    req_axis_v = [req_axis.x, req_axis.y, req_axis.z,
                  req_axis.roll, req_axis.pitch, req_axis.yaw]

    setted_axis_v = [setted_axis.x, setted_axis.y, setted_axis.z,
                     setted_axis.roll, setted_axis.pitch, setted_axis.yaw]

    is_matched = True
    i = 0
    while is_matched and i < 6:
        if not(req_axis_v[i]):  # If in some dof both are false (both active)
            if not(setted_axis_v[i]):
                is_matched = False
        i += 1

    return is_matched


def merge(sp, name):
    """ This function merge all requests in sp. This is used by iterate """
    lock.acquire()

    # Initialize default output values
    v = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    disable_axis = Bool6Axis()
    disable_axis.x = True
    disable_axis.y = True
    disable_axis.z = True
    disable_axis.roll = True
    disable_axis.pitch = True
    disable_axis.yaw = True

    # Goal
    goal = GoalDescriptor()
    goal.priority = GoalDescriptor.PRIORITY_LOW
    goal.requester = name

    sp = sort_responses(sp)

    if len(sp) == 0:
        rospy.loginfo("/merge: no merged responses")

    elif len(sp) == 1:
        rospy.loginfo("/merge: one merged response")
        disable_axis = sp[0].disable_axis
        goal = sp[0].goal
        if 'twist' in dir(sp[0]):
            v = get_twist(sp[0])
        elif 'wrench' in dir(sp[0]):
            v = get_wrench(sp[0])
        elif 'altitude' in dir(sp[0]):
            v = get_pose(sp[0])
        else:
            rospy.logerr('/merge: invalid type!')

    else:
        # The output goal is set to the highest priority in sp
        goal = sp[0].goal

        for resp in sp:
            if 'twist' in dir(resp):
                v_sp = get_twist(resp)
            elif 'wrench' in dir(resp):
                v_sp = get_wrench(resp)
            elif 'altitude' in dir(resp):
                v_sp = get_pose(resp)
            else:
                rospy.logerr('/merge: invalid type!')
                v_sp = v

            # If all active dofs are not already active
            if match(resp.disable_axis, disable_axis):
                # Add new dofs
                if not(resp.disable_axis.x):
                    disable_axis.x = False
                    v[0] = v_sp[0]
                if not(resp.disable_axis.y):
                    disable_axis.y = False
                    v[1] = v_sp[1]
                if not(resp.disable_axis.z):
                    disable_axis.z = False
                    v[2] = v_sp[2]
                if not(resp.disable_axis.roll):
                    disable_axis.roll = False
                    v[3] = v_sp[3]
                if not(resp.disable_axis.pitch):
                    disable_axis.pitch = False
                    v[4] = v_sp[4]
                if not(resp.disable_axis.yaw):
                    disable_axis.yaw = False
                    v[5] = v_sp[5]

        rospy.loginfo("/merge: %d merged responses", len(sp))

    lock.release()
    return [v, disable_axis, goal]


def get_pose(wwr):
    """ This function returns pose values. This is used by merge """
    if wwr.altitude_mode:
        return [wwr.position.north, wwr.position.east,
                wwr.altitude, wwr.orientation.roll,
                wwr.orientation.pitch, wwr.orientation.yaw]
    else:
        return [wwr.position.north, wwr.position.east,
                wwr.position.depth, wwr.orientation.roll,
                wwr.orientation.pitch, wwr.orientation.yaw]


def set_pose(wwr, v):
    """ This function is used to put the input to a wwr message """
    wwr.position.north = v[0]
    wwr.position.east = v[1]
    wwr.orientation.roll = cola2_lib.wrapAngle(v[3])
    wwr.orientation.pitch = cola2_lib.wrapAngle(v[4])
    wwr.orientation.yaw = cola2_lib.wrapAngle(v[5])

    if wwr.altitude_mode:
        wwr.altitude = v[2]
    else:
        wwr.position.depth = v[2]


def get_twist(bvr):
    """ This function returns velocity values. This is used by merge """
    return [bvr.twist.linear.x, bvr.twist.linear.y, bvr.twist.linear.z,
            bvr.twist.angular.x, bvr.twist.angular.y, bvr.twist.angular.z]


def set_twist(bvr, v0, v1):
    """ This function is used to put the input to a bvr message """
    ret = [0, 0, 0, 0, 0, 0]
    for i in range(6):
        if v0[i] * v1[i] > 0:
            # Both are positive or negative, the do an average
            ret[i] = (v0[i] + v1[i]) / 2.0
        else:
            # Different signs
            ret[i] = v0[i] + v1[i]

    bvr.twist.linear.x = ret[0]
    bvr.twist.linear.y = ret[1]
    bvr.twist.linear.z = ret[2]
    bvr.twist.angular.x = ret[3]
    bvr.twist.angular.y = ret[4]
    bvr.twist.angular.z = ret[5]


def get_wrench(bfr):
    """ This function returns force values. This is used by merge """
    return [bfr.wrench.force.x, bfr.wrench.force.y, bfr.wrench.force.z,
            bfr.wrench.torque.x, bfr.wrench.torque.y, bfr.wrench.torque.z]


def set_wrench(bfr, v0, v1):
    """ This function is used to put the input to a bfr message """

    ret = [0, 0, 0, 0, 0, 0]
    for i in range(6):
        if v0[i] * v1[i] > 0:
            # Both are positive or negative, the do an average
            ret[i] = (v0[i] + v1[i]) / 2.0
        else:
            # Different signs
            ret[i] = v0[i] + v1[i]

    bfr.wrench.force.x = ret[0]
    bfr.wrench.force.y = ret[1]
    bfr.wrench.force.z = ret[2]
    bfr.wrench.torque.x = ret[3]
    bfr.wrench.torque.y = ret[4]
    bfr.wrench.torque.z = ret[5]


def iterate(sp, name):
    """ This is the main function. This is called by the specific merge """
    # Remove all the messages remove those whose time stamps
    # has expired (> EXPIRE_TIME second) """
    lock.acquire()

    # Actual time
    now = rospy.Time.now()

    # For each in sp
    for m in sp:
        # If time difference is greater than EXPIRE_TIME sec, 
        # delete it from sp
        if (now - m.header.stamp).to_sec() > EXPIRE_TIME:
            sp.remove(m)

    lock.release()

    # Return merged response!
    return merge(sp, name)  


def are_body_velocity_req_equal(bvr1, bvr2):
    """ If both body_velocity_req messages are equal except the header
        returns True. Otherwise if the messages are different or  if more 
        than 0.1s has happen between both messages returns False."""
        
    if (bvr1.twist == bvr2.twist and bvr1.disable_axis == bvr2.disable_axis 
        and bvr1.goal.priority == bvr2.goal.priority):
        # The response has not changed
        if bvr1.header.stamp.to_sec() - bvr2.header.stamp.to_sec() >= MINIMUM_PERIOD:
            # More than 0.1s since last response
            return False
        else:
            return True
    else:
        return False
        
        
def are_world_waypoint_req_equal(wwr1, wwr2):
    """ If both world_waypoint_req messages are equal except the header
        returns True. Otherwise if the messages are different or  if more 
        than 0.1s has happen between both messages returns False."""
        
    if (wwr1.position == wwr2.position and
        wwr1.orientation == wwr2.orientation and
        wwr1.disable_axis == wwr2.disable_axis and 
        wwr1.goal.priority == wwr2.goal.priority):
        # The response has not changed
        if wwr1.header.stamp.to_sec() - wwr2.header.stamp.to_sec() >= MINIMUM_PERIOD:
            # More than 0.1s since last response
            return False
        else:
            return True
    else:
        return False
