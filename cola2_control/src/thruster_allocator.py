#!/usr/bin/env python
"""@@This node is used to convert from forces to thruster setpoints.@@"""

# ROS imports
import roslib
roslib.load_manifest('cola2_control')
import rospy

# Msgs imports
from cola2_control.msg import Setpoints
from auv_msgs.msg import BodyForceReq
from std_srvs.srv import Empty, EmptyResponse

# Python imports
from numpy import array, matrix, clip, asarray, zeros, squeeze, size

# Custom imports
from cola2_lib import cola2_ros_lib
from cola2_lib.diagnostic_helper import DiagnosticHelper

class ThrusterAllocator:
    """ Translate from forces to thrusters setpoints  """

    def __init__(self, name):
        """ Constructor  """
        self.name = name

        # Set up diagnostics: name, hw_id, freq, max_freq_error%
        self.diagnostic = DiagnosticHelper(self.name, "soft", 10, 10)

        # Enable parameter
        self.enable = True

        # Default parameters
        self.n_thrusters = 3                        # number of thrusters
        self.tpl = [0, 1]                           # linear thrusters model
        self.force_to_thrusters_ratio = 30.0        # ratio
        self.asymmetry = 1.3                        # Backwards force asymmetry

        # Load parameters
        self.get_config()

        # Create publisher
        self.pub_thrusters = rospy.Publisher("/cola2_control/thrusters_data", Setpoints)

        # Create Subscriber
        rospy.Subscriber("/cola2_control/merged_body_force_req", BodyForceReq, self.main_callback)

        # Create services
        self.e_srv = rospy.Service('/cola2_control/enable_thrusters', Empty, self.enable_srv)
        self.d_srv = rospy.Service('/cola2_control/disable_thrusters', Empty, self.disable_srv)

        # Show message
        rospy.loginfo("%s: initialized", self.name)


    def main_callback(self, data) :
        """ Callback and main function """
        if ( self.enable ) :
            # Input data
            body_force_req = zeros(6)

            if not data.disable_axis.x :
                body_force_req[0] = data.wrench.force.x
            if not data.disable_axis.y :
                body_force_req[1] = data.wrench.force.y
            if not data.disable_axis.z :
                body_force_req[2] = data.wrench.force.z
            if not data.disable_axis.roll :
                body_force_req[3] = data.wrench.torque.x
            if not data.disable_axis.pitch :
                body_force_req[4] = data.wrench.torque.y
            if not data.disable_axis.yaw :
                body_force_req[5] = data.wrench.torque.z

            #rospy.loginfo("%s: body force request: %s", self.name, str(body_force_req))

            # Merge Surge and Yaw DoF giving priority to YAW
            body_force_req = self.merge_surge_yaw(body_force_req)

            # Computes the force to be done for each actuator
            force = self.tcm_inv * matrix(body_force_req).T
            force = squeeze(asarray(force)).ravel() # Matrix to 1D array

            ##########################################################################
            # TODO: Transform force for each thruster to setpoint using a model

            # Linearize actuator forces
            #rospy.loginfo("%s: force per thruster: %s", self.name, str(force))

            # Adjust Newtons to setpoints
            setpoints = self.force_to_setpoints(force)
            setpoints = self.polynomial_model_to_actuators_force(setpoints)
            #rospy.loginfo("%s: thrusters setpoints: %s", self.name, str(setpoints))
           ###########################################################################

            # Publish computed data
            thrusters = Setpoints()
            thrusters.header.stamp = rospy.Time.now()
            thrusters.header.frame_id = self.frame_id
            thrusters.setpoints = list( array( setpoints ).ravel() )
            self.pub_thrusters.publish(thrusters)
            self.diagnostic.check_frequency()


    def merge_surge_yaw(self, data):
        """ If the composition of Surge (v[0]) and Yaw (v[5]) overrides
        the maximum force per thruster,  Yaw is respected and Surge is
        reduced. """
        if abs(data[0]) + abs(data[5]/self.thruster_distance) > self.max_force_surge_yaw:
            if data[0] > 0.0:
                data[0] = self.max_force_surge_yaw - abs(data[5]/self.thruster_distance)
                if data[0] < 0.0:
                    data[0] = 0.0
            else:
                data[0] = -self.max_force_surge_yaw + abs(data[5]/self.thruster_distance)
                if data[0] > 0.0:
                    data[0] = 0.0
        return data


    def polynomial_model_to_actuators_force(self, data) :
        """ Compute polynomial correction to thruster forces """
        ret = zeros( len(data) ) # I use size() to avoid len() of a number

        for th in range( len(data) ) :
            if data[th] < 0.0 :
                th_value = abs(data[th])
                change_sign = True
            else :
                th_value = data[th]
                change_sign = False

            for it in range( len(self.tpl) ) :
                ret[th] = ret[th] + pow( th_value, self.tple[it] ) * self.tpl[it]

            if change_sign:
                ret[th] = ret[th] * (-1.0)

        return ret


    def force_to_setpoints(self, data) :
        """ Compute newtons to setpoints for each thruster """
        # TODO: compute models for each actuator instead of using simple ratio
        setpoints = asarray(data)/self.force_to_thrusters_ratio

        for i in range(len(setpoints)):
            # Apply asymmetry
            if setpoints[i] < 0:
                setpoints[i] *= self.asymmetry

            # Saturate
            if setpoints[i] > self.max_setpoint[i]:
                setpoints[i] = self.max_setpoint[i]
            if setpoints[i] < self.min_setpoint[i]:
                setpoints[i] = self.min_setpoint[i]

        return setpoints.clip(min=-1, max=1)


    def enable_srv(self, req):
        """ Enable thruster service """
        self.enable = True
        rospy.loginfo('%s: enabled', self.name)
        return EmptyResponse()


    def disable_srv(self, req):
        """ Disable thruster service """
        self.enable = False
        rospy.loginfo('%s: disabled', self.name)
        return EmptyResponse()


    def get_config(self) :
        """ Get config from the rosparam server """
        param_dict = {'frame_id': 'thruster_allocator/frame_id',
                      'n_thrusters': 'thruster_allocator/n_thrusters',
                      'tpl': 'thruster_allocator/thrusters_polynomial_linearization',
                      'tple': 'thruster_allocator/thrusters_polynomial_linearization_exp',
                      'force_to_thrusters_ratio': 'thruster_allocator/force_to_thrusters_ratio',
                      'max_setpoint': 'thruster_allocator/max_setpoint',
                      'min_setpoint': 'thruster_allocator/min_setpoint',
                      'max_force_surge_yaw': 'thruster_allocator/max_force_surge_plus_yaw',
                      'tcm': 'thruster_allocator/thruster_control_matrix',
                      'asymmetry': 'thruster_allocator/asymmetry'}

        if not cola2_ros_lib.getRosParams(self, param_dict, self.name):
            rospy.logfatal("%s: shutdown due to invalid config parameters!", self.name)
            exit(0)  # TODO: find a better way

        # Resize...
        self.tcm = list(array(self.tcm).ravel())
        self.tcm = matrix(array(self.tcm).reshape(6, size(self.tcm) / 6))
        self.tcm_inv = self.tcm.I
        self.thruster_distance = matrix(abs(self.tcm[5, 0:size(self.tcm, 1)])).max()


if __name__ == '__main__':
    try:
        rospy.init_node('thruster_allocator')
        thruster_allocator = ThrusterAllocator( rospy.get_name() )
        rospy.spin()
    except rospy.ROSInterruptException: pass
