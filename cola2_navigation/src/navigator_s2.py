#!/usr/bin/env python
"""@@The navigator subscribes to sensor drivers, interacts with ekf_slam node,
and publishes navigation status: position, velocities and more.@@"""

# ROS imports
import roslib
roslib.load_manifest('cola2_navigation')
import rospy
import tf
from tf.transformations import euler_from_quaternion
import PyKDL
from cola2_lib import cola2_lib, cola2_ros_lib, NED

# Msgs imports
from nav_msgs.msg import Odometry
from cola2_navigation.msg import FastraxIt500Gps
from cola2_navigation.msg import LinkquestDvl
from cola2_navigation.msg import TeledyneExplorerDvl
from cola2_navigation.msg import PressureSensor
from sensor_msgs.msg import Imu
from sensor_msgs.msg import Range

from auv_msgs.msg import NavSts
from cola2_safety.msg import NavSensorsStatus
from geometry_msgs.msg import PoseWithCovarianceStamped
from geometry_msgs.msg import TwistWithCovarianceStamped
from pose_ekf_slam.srv import SetPositionRequest, SetPosition
from cola2_navigation.srv import SetNedOrigin, SetNedOriginResponse
from cola2_navigation.srv import SetGPS, SetGPSResponse
from std_srvs.srv import Empty, EmptyResponse, EmptyRequest
from cola2_safety.srv import RecoveryAction, RecoveryActionRequest, RecoveryActionResponse
from cola2_lib.diagnostic_helper import DiagnosticHelper
from diagnostic_msgs.msg import DiagnosticStatus

# Python imports
from numpy import median, array, math, cross, dot, matrix


INVALID_ALTITUDE = -3.2665  # This altitude is the initial one, before any
                            # altitude update has been made


class NavigatorS2:
    """ The navigator converts messages from drivers to updates to the EKF,
        and process the navigation data that comes back from the filter """

    def __init__(self, name):
        """ Init. the class """
        self.name = name

        # Services for disable thrusters and recovery actions
        rospy.loginfo("%s: waiting for services", self.name)
        try:
            rospy.wait_for_service('/cola2_control/disable_thrusters', 20)
            self.abort_thrusters_srv = rospy.ServiceProxy(
                                '/cola2_control/disable_thrusters', Empty)
        except rospy.exceptions.ROSException:
            rospy.logfatal('%s: error creating client to disable thrusters',
                         self.name)
            self.no_disable_thrusters_service_timer = rospy.Timer(rospy.Duration(0.4), self.no_disable_thrusters_message)
        try:
            rospy.wait_for_service('/cola2_safety/recovery_action', 30)
            self.recover_action_srv = rospy.ServiceProxy(
                                '/cola2_safety/recovery_action', RecoveryAction)
        except rospy.exceptions.ROSException:
            rospy.logfatal('%s: error creating client to recovery action',
                         self.name)
            self.no_recovery_actions_message_timer = rospy.Timer(rospy.Duration(0.4), self.no_recovery_actions_message)
            try:
                self.abort_thrusters_srv(EmptyRequest())
            except:
                rospy.logfatal('%s: error disabling thrusters', self.name)

        # Set up diagnostics
        self.diagnostic = DiagnosticHelper(self.name, "software")

        # tf listener
        self.listener = tf.TransformListener()

        # Create msgs
        self.nav = NavSts()
        self.imu = Imu()
        self.last_gps = FastraxIt500Gps()

        # Get config
        self.get_config()

        # Initialize flags
        self.init_lat = 0.0
        self.init_lon = 0.0

        # Initialize altitude with an invalid one
        self.altitude = INVALID_ALTITUDE
        self.bad_altitude_counter = 0
        self.bad_dvl_data = 0
        self.gps_keep_updating = True
        self.bottom_status = 0

        # Init last sensor update
        self.init_time = rospy.Time.now()
        self.dvl_last_update = self.init_time
        self.imu_last_update = self.init_time
        self.last_imu_update = self.init_time
        self.dvl_init = False
        self.imu_init = False
        self.gps_init = not self.gps_update  # gps_update is a param in config
        self.ekf_init = False
        self.detected_warnings = 0
        self.imu_data = Imu()

        # These variables are used to check dvl provenance
        self.linkquest_frames = []
        self.rdi_frames = []

        # Buffers & coef.
        self.gps_init_samples_list = []

        # Init slam and gps
        if self.gps_init:
            if self.fixed_ned:  # fixed_ned is a param in config
                self.init_lat = self.ned_lat  # ned_lat is a param in config
                self.init_lon = self.ned_lon  # ned_lon is a param in config
                self.ned = NED.NED(self.ned_lat, self.ned_lon, 0.0)
                self.set_EKF_slam(0.0, 0.0, 0.0)
            else:
                while not rospy.is_shutdown():
                    rospy.logerr('%s: DEFINE FIXED NED OR ENABLE GPS RECEIVER!')
                    rospy.sleep(0.4)

        # Create publisher
        self.pub_imu = rospy.Publisher("/pose_ekf_slam/imu_input", Imu)
        self.pub_vel = rospy.Publisher("/pose_ekf_slam/velocity_update",
                                       TwistWithCovarianceStamped)
        self.pub_pose = rospy.Publisher("/pose_ekf_slam/pose_update",
                                        PoseWithCovarianceStamped)
        self.pub_nav_sts = rospy.Publisher("/cola2_navigation/nav_sts",
                                           NavSts)
        self.pub_nav_sensors_status = rospy.Publisher("/cola2_safety/nav_sensors_status",
                                                      NavSensorsStatus)

        # Create subscriber
        rospy.Subscriber("/pose_ekf_slam/odometry", Odometry,
                         self.update_odometry)  # This comes back from EKF
        rospy.Subscriber("/cola2_navigation/linkquest_navquest600_dvl",
                         LinkquestDvl, self.update_linkquest_DVL)
        rospy.Subscriber("/cola2_navigation/teledyne_explorer_dvl",
                         TeledyneExplorerDvl,
                         self.update_rdi_DVL)
        rospy.Subscriber("/cola2_navigation/imu", Imu, self.update_IMU)
        rospy.Subscriber("/cola2_navigation/pressure_sensor", PressureSensor, self.update_pressure)

        if self.gps_update:
            rospy.Subscriber("/cola2_navigation/fastrax_it_500_gps",
                             FastraxIt500Gps, self.update_GPS)

        # Create services
        self.reset_navigation = rospy.Service('/cola2_navigation/reset_navigation',
                                              Empty, self.reset_navigation)
        self.reset_gps_navigation = rospy.Service('/cola2_navigation/set_gps_navigation',
                                                  SetGPS, self.set_GPS_navigation)

        self.reset_ned_origin = rospy.Service('/cola2_navigation/set_ned_origin',
                                                  SetNedOrigin, self.set_ned_origin)

        # Timer for check_sensors method
        rospy.Timer(rospy.Duration(self.check_sensors_period),
                    self.check_sensors)

        rospy.loginfo("%s: initialized", self.name)


    def set_EKF_slam(self, x, y, z):
        """ This method is used to do a change in EKF position (not an
            update) using a service. """
        try:
            rospy.wait_for_service('/pose_ekf_slam/set_position', 5)
            set_pose = rospy.ServiceProxy('pose_ekf_slam/set_position',
                                          SetPosition)
            sp = SetPositionRequest()
            sp.position.x = x
            sp.position.y = y
            sp.position.z = z
            set_pose(sp)

        except rospy.exceptions.ROSException:
            rospy.logerr('%s: error initializing pose_ekf_slam', self.name)


    def reset_navigation(self, req):  # This is a service
        """ This method is used to set the actual pose as the initial """
        # Show a message
        rospy.loginfo("%s: reset navigation", self.name)

        # Set actual position as initial position
        self.init_lat = self.nav.global_position.latitude
        self.init_lon = self.nav.global_position.longitude

        # Set the actual (now initial) pose to the ned
        self.ned = NED.NED(self.init_lat,
                           self.init_lon,
                           self.nav.position.depth)

        # As we are in the initial x y pose, we set the EKF like follows
        self.set_EKF_slam(0.0, 0.0, self.nav.position.depth)

        return EmptyResponse()


    def set_GPS_navigation(self, req):  # This is a service
        """ This method is used to actualize the ned pose of the EKF (not
            through an update) using the GPS. The initial pose of the ned
            is not changed """
        # Display a message
        rospy.loginfo("%s: set GPS navigation to:\n%s", self.name, req)

        # Define a response message
        ret = SetGPSResponse()

        # Check if there is a fresh gps data
        if (rospy.Time.now().to_sec() - self.last_gps.header.stamp.to_sec() < 2.0):
            if (self.last_gps.data_quality >= 1 and
                 self.last_gps.latitude_hemisphere >= 0 and
                 self.last_gps.longitude_hemisphere >= 0 and
                 self.last_gps.h_dop > 0.0 and  # TODO: be aware of this kind of checks
                 self.last_gps.h_dop < 2.0):

                # Get degree only representation of last GPS data
                current_lat, current_lon = NED.degreeMinute2Degree(
                                        self.last_gps.latitude,
                                        self.last_gps.longitude)

                # Get the last GPS pose in the ned
                pose = self.ned.geodetic2ned([current_lat,
                                              current_lon,
                                              self.nav.position.depth])

                # Compute the distance between the EKF pose and the last GPS one
                distance = ((self.nav.position.north - pose[0]) ** 2 +
                            (self.nav.position.east - pose[1]) ** 2) ** 0.5

                # Reinitialize EKF-SLAM
                self.set_EKF_slam(pose[0], pose[1], self.nav.position.depth)

                ret.success = "Navigation initialized with north: {0}, east: {1}. Distance to previous nav {2}".format(current_lat, current_lon, distance)
            else:
                ret.success = 'Impossible to update Navigator with GPS. Bad data quality found'
        else:
            ret.success = 'Impossible to update Navigator with GPS. No data found'
        return ret


    def set_ned_origin(self, req):
        """ This method is a callback of a service used to change NED origin """
        rospy.loginfo("%s: set NED origin at %s", self.name, req)

        # Check if it is possible to reset NED origin
        if (rospy.Time.now().to_sec() - self.last_gps.header.stamp.to_sec() < 2.0):
            if (self.last_gps.data_quality >= 1 and
                self.last_gps.latitude_hemisphere >= 0 and
                self.last_gps.longitude_hemisphere >= 0 and
                self.last_gps.h_dop > 0.0 and
                self.last_gps.h_dop < 2.0):

                # Overwrite new NED origin
                self.init_lat = req.latitude
                self.init_lon = req.longitude

                self.ned = NED.NED(self.init_lat,
                           self.init_lon,
                           self.nav.position.depth)

                # Compute current lat / long using new NED
                current_lat, current_lon = NED.degreeMinute2Degree(
                                        self.last_gps.latitude,
                                        self.last_gps.longitude)

                pose = self.ned.geodetic2ned([current_lat,
                                              current_lon,
                                              self.nav.position.depth])

                self.set_EKF_slam(pose[0] ,pose[1], self.nav.position.depth)

                return SetNedOriginResponse(True)

        return SetNedOriginResponse(False)


    def check_sensors(self, event):
        """ This method is the callback of a timer. Here a check of all
            sensors is done """
        # Check if all sensors are init
        if (self.dvl_init and self.imu_init and self.ekf_init):
            # NavStatus message
            now = rospy.Time.now()
            nav_sensors_status = NavSensorsStatus()
            nav_sensors_status.header.stamp = now

            warning = False
            if ((now - self.dvl_last_update).to_sec() >
                 self.dvl_max_period_error):
                nav_sensors_status.dvl_status = False
                warning = True
                self.diagnostic.add("DVL_status", "Error")
            else:
                nav_sensors_status.dvl_status = True
                self.diagnostic.add("DVL_status", "Ok")

            if ((now - self.imu_last_update).to_sec() >
                 self.imu_max_period_error):
                nav_sensors_status.imu_status = False
                warning = True
                self.diagnostic.add("IMU_status", "Error")
            else:
                nav_sensors_status.imu_status = True
                self.diagnostic.add("IMU_status", "Ok")

            # Check for bad dvl data
            if self.bad_dvl_data > 20:
                rospy.logfatal("%s: bad DVL data. Emergency surface!", self.name)
                # TODO: Next line was commented otherwise was impossible to operate SPARUS
                self.recover_action_srv(RecoveryActionRequest.ABORT_AND_SURFACE)

            # Publish diagnostic data
            self.diagnostic.add("DVL_init", str(self.dvl_init))
            self.diagnostic.add("IMU_init", str(self.imu_init))
            self.diagnostic.add("EKF_init", str(self.ekf_init))
            if warning:
                self.detected_warnings = self.detected_warnings + 1
                if self.detected_warnings < 5:
                    self.diagnostic.setLevel(DiagnosticStatus.WARN)
                else:
                    self.diagnostic.setLevel(DiagnosticStatus.ERROR)

                    # Lost IMU or DVL: go up without navigation!
                    rospy.logfatal("%s: robot has lost some sensors. Emergency surface!", self.name)
                    self.recover_action_srv(RecoveryActionRequest.EMERGENCY_SURFACE)
            else:
                self.detected_warnings = 0
                self.diagnostic.setLevel(DiagnosticStatus.OK)

            # Publish nav_sensors_status message. This message gives runtime
            # information about DVL and IMU, once initialized.
            self.pub_nav_sensors_status.publish(nav_sensors_status)
        else:
            # Publish diagnostic data
            self.diagnostic.add("DVL_init", str(self.dvl_init))
            self.diagnostic.add("IMU_init", str(self.imu_init))
            self.diagnostic.add("EKF_init", str(self.ekf_init))
            self.diagnostic.setLevel(DiagnosticStatus.WARN, 'Initializing...')

            if (rospy.Time.now() - self.init_time).to_sec() > self.max_init_time:
                rospy.logfatal("%s: not all nav sensors have been initialized!", self.name)
                rospy.logfatal("%s: DVL init: %s", self.name, self.dvl_init)
                rospy.logfatal("%s: IMU init: %s", self.name, self.imu_init)
                rospy.logfatal("%s: EKF init: %s", self.name, self.ekf_init)
            else:
                rospy.loginfo("%s: initializing nav sensors", self.name)


    def update_odometry(self, data):
        """ This is the callback from the EKF """
        # Set navigator flag to true
        self.ekf_init = True

        # Fill nav status topic
        self.nav.header.stamp = rospy.Time.now()
        self.nav.header.frame_id = self.robot_frame_name  # This is a param

        # Store ned origin
        self.nav.origin.latitude = self.init_lat
        self.nav.origin.longitude = self.init_lon

        # Store actual pose
        self.nav.position.north = data.pose.pose.position.x
        self.nav.position.east = data.pose.pose.position.y
        self.nav.position.depth = data.pose.pose.position.z

        # Get global pose of actual pose
        pose = self.ned.ned2geodetic(array([self.nav.position.north,
                                            self.nav.position.east,
                                            self.nav.position.depth]))
        self.nav.global_position.latitude = pose[0]
        self.nav.global_position.longitude = pose[1]

        # Store velocity
        self.nav.body_velocity.x = data.twist.twist.linear.x
        self.nav.body_velocity.y = data.twist.twist.linear.y
        self.nav.body_velocity.z = data.twist.twist.linear.z

        # Get euler angles from quaternion
        angle = euler_from_quaternion([data.pose.pose.orientation.x,
                                       data.pose.pose.orientation.y,
                                       data.pose.pose.orientation.z,
                                       data.pose.pose.orientation.w])
        self.nav.orientation.roll = angle[0]
        self.nav.orientation.pitch = angle[1]
        self.nav.orientation.yaw = angle[2]

        # Store rates
        self.nav.orientation_rate.roll = data.twist.twist.angular.x
        self.nav.orientation_rate.pitch = data.twist.twist.angular.y
        self.nav.orientation_rate.yaw = data.twist.twist.angular.z

        # Store altitude
        self.nav.altitude = self.altitude

        # Store covariances
        self.nav.position_variance.north = data.pose.covariance[0]
        self.nav.position_variance.east = data.pose.covariance[7]
        self.nav.position_variance.depth = data.pose.covariance[14]
        self.nav.orientation_variance.roll = self.imu.orientation_covariance[0]
        self.nav.orientation_variance.pitch = self.imu.orientation_covariance[4]
        self.nav.orientation_variance.yaw = self.imu.orientation_covariance[8]

        # Publish navigation message
        self.pub_nav_sts.publish(self.nav)

        # Publish sparus2 tf
        br = tf.TransformBroadcaster()
        br.sendTransform((self.nav.position.north,
                          self.nav.position.east,
                          self.nav.position.depth),
                         (data.pose.pose.orientation.x,
                          data.pose.pose.orientation.y,
                          data.pose.pose.orientation.z,
                          data.pose.pose.orientation.w),
                         self.nav.header.stamp,
                         self.nav.header.frame_id,
                         self.world_frame_name)

        # Publish altitude tf
        # TODO: altitude tf is fixed in the code!!!
        br = tf.TransformBroadcaster()
        br.sendTransform((0.0, 0.0, 0.0),
                         tf.transformations.quaternion_from_euler(0.0, 0.0, 0.0, 'sxyz'),
                         self.nav.header.stamp,
                         'altitude',
                         self.nav.header.frame_id)


    def update_pressure(self, data):
        """ This method is the presure_sensor callback, input is in bars """
        # TODO: compensate for sensor position
        # Prepare a pose message
        p = PoseWithCovarianceStamped()
        p.header.stamp = rospy.Time.now()
        p.header.frame_id = self.robot_frame_name

        # Check input data and create a new pose
        if data.pressure < -0.5:
            rospy.logfatal('%s: pressure sensor probably not connected', self.name)
            p.pose.pose.position.z = 0.0
        else:
            p.pose.pose.position.z = data.pressure * self.bar_to_meters_depth + self.depth_correction
        p.pose.pose.position.x = 0.0
        p.pose.pose.position.y = 0.0
        p.pose.covariance[0] = 1e10
        p.pose.covariance[7] = 1e10
        p.pose.covariance[14] = 0.3

        # Do a pose update
        self.pub_pose.publish(p)


    def update_GPS(self, gps):
        """ This method is the callback of the GPS """
        # Store the GPS input data to self.last_gps. This is used by
        # set_GPS_navigation method
        self.last_gps = gps

        # Check the quality of the data
        if (gps.data_quality >= 1 and
             gps.latitude_hemisphere >= 0 and  # TODO: be aware of this kind of checks
             gps.longitude_hemisphere >= 0 and
             gps.h_dop > 0.0 and
             gps.h_dop < 2.0 and
             self.gps_keep_updating):

            if not self.gps_init:  # If GPS not initialized
                # Get lat and lon in degree only fromat
                lat_deg, lon_deg = NED.degreeMinute2Degree(gps.latitude,
                                                           gps.longitude)

                # Append recieved data to gps_init_samples_list
                self.gps_init_samples_list.append([lat_deg, lon_deg])

                if len(self.gps_init_samples_list) >= self.gps_init_samples:
                    # If there are enough GPS fixes, compute median filter
                    [self.init_lat, self.init_lon] = median(array(self.gps_init_samples_list), axis=0)

                    # Show a message with initial GPS pose
                    rospy.loginfo('%s: GPS init data:\nlat: %s\nlon: %s',
                                  self.name, self.init_lat, self.init_lon)

                    # Set navigator init flag to true
                    self.gps_init = True

                    # Test if only one gps update is needed
                    if self.only_one_gps_update:
                        self.gps_keep_updating = False
                        rospy.loginfo('%s: only one GPS init', self.name)

                    if self.fixed_ned:  # Fixed ned with GPS
                        # Init ned with ned_lat and ned_lon
                        self.ned = NED.NED(self.ned_lat, self.ned_lon, 0.0)

                        # Convert geodetic GPS pose to ned pose and set this
                        # pose to the EKF
                        pose = self.ned.geodetic2ned(
                            [self.init_lat, self.init_lon, 0.0])
                        self.set_EKF_slam(pose[0], pose[1], 0.0)

                        # Initial pose is ned_lat and ned_lon
                        self.init_lat = self.ned_lat
                        self.init_lon = self.ned_lon
                    else:  # GPS without fixed ned
                        # Init ned with GPS data
                        self.ned = NED.NED(self.init_lat, self.init_lon, 0.0)

                        # The starting pointis this inital GPS data
                        self.set_EKF_slam(0.0, 0.0, 0.0)
                # TODO: maybe it would be better to force all the initial data
                # to be consecutive in time
            else:
                rospy.loginfo('%s: updating gps', self.name)

                # TODO: check how we can improve this control here!
                # Compute euclidean distance
                lat_deg, lon_deg = NED.degreeMinute2Degree(gps.latitude,
                                                           gps.longitude)

                # Get ned pose
                pose = self.ned.geodetic2ned([lat_deg,
                                              lon_deg,
                                              self.nav.position.depth])

                # Compute 2D distance between EKF and GPS data
                distance = ((self.nav.position.north - pose[0]) ** 2 +
                            (self.nav.position.east - pose[1]) ** 2) ** 0.5

                if distance < 50.0:
                    # If the error between the filter and the GPS is small,
                    # update the kalman filter
                    p = PoseWithCovarianceStamped()
                    p.header.stamp = gps.header.stamp
                    p.header.frame_id = self.robot_frame_name

                    # Create a new pose
                    p.pose.pose.position.x = pose[0]
                    p.pose.pose.position.y = pose[1]
                    p.pose.pose.position.z = 0.0

                    # Only the number in the covariance matrix diagonal
                    # are used for the updates!
                    # Example of X, Y update without modifying the Z
                    p.pose.covariance[0] = 1.0
                    p.pose.covariance[7] = 1.0
                    p.pose.covariance[14] = 1e10  # TODO: there should be another way to do this...

                    # Do a pose update
                    self.pub_pose.publish(p)
                else:
                    # If the error is huge, we assume that the GPS data
                    # is not valid
                    rospy.loginfo('%s: large distance (%s) when trying GPS update!',
                                  self.name,
                                  distance)


    def update_linkquest_DVL(self, dvl):
        """ This method is the callback of the linkquest DVL """
        # Store dvl_last_update var, used by check_sensors method
        self.dvl_last_update = rospy.Time.now()

        # Check dvl provenance
        if not dvl.header.frame_id in self.linkquest_frames:
            self.linkquest_frames.append(dvl.header.frame_id)
            rospy.loginfo('%s: using Linkquest DVL data from %s', self.name, dvl.header.frame_id)
        if len(self.linkquest_frames) > 1:
            rospy.logwarn('%s: Linkquest DVL data comes from more than one place', self.name)
        if len(self.rdi_frames) > 0:
            rospy.loginfo('%s: using data from both Linkquest and RDI DVL', self.name)

        # Check data
        if dvl.velocityInstFlag == 1:
            rospy.loginfo('%s: bottom DVL', self.name)
            input_velocity = dvl.velocityInst
            input_covariance = 0.10  # TODO: Config file
            self.diagnostic.add("DVL_data", "bottom")
            self.bad_dvl_data = 0
        elif dvl.waterVelocityInstFlag == 2:
            self.bad_dvl_data = self.bad_dvl_data + 1
            rospy.loginfo('%s: water DVL. Bad data: %s', self.name, self.bad_dvl_data)
            return
            input_velocity = dvl.waterVelocityInst
            input_covariance = 0.80
            self.diagnostic.add("DVL_data", "water")
        else:
            self.bad_dvl_data = self.bad_dvl_data + 1
            rospy.loginfo('%s: invalid DVL velocity msg. Bad data: %s', self.name, self.bad_dvl_data)
            return

        # Set navigator flag to true
        self.dvl_init = True

        # Init vars
        vel = PyKDL.Vector(0.0,
                           0.0,
                           0.0)
        R = PyKDL.Rotation.RPY(0.0, 0.0, -math.pi/4.0)  # TODO: Rotate the DVL properly. Config file. Rotate covariances. Take a look at motion transformations file
        trans = array([-0.41327, 0.0, 0.09229])

        # Define a velocity message
        msg = TwistWithCovarianceStamped()
        msg.header.stamp = dvl.header.stamp
        msg.header.frame_id = "sparus2"  # THIS MEANS THE CONVERSION IS HERE!

        # Using the following if-else statements some filtering is done
        vel[0] = - input_velocity[0]
        msg.twist.covariance[0] = input_covariance
        if (abs(vel[0]) > self.dvl_max_v):  # Some filtering here (max velocity of the transducer)
            vel[0] = cola2_lib.saturateValueFloat(vel[0], self.dvl_max_v)
            msg.twist.covariance[0] = input_covariance + 0.20

        vel[1] = - input_velocity[1]
        msg.twist.covariance[7] = input_covariance
        if (abs(vel[1]) > self.dvl_max_v):
            vel[1] = cola2_lib.saturateValueFloat(vel[1], self.dvl_max_v)
            msg.twist.covariance[7] = input_covariance + 0.20

        vel[2] = - input_velocity[2]
        msg.twist.covariance[14] = input_covariance
        if (abs(vel[2]) > self.dvl_max_v):
            vel[2] = cola2_lib.saturateValueFloat(vel[2], self.dvl_max_v)
            msg.twist.covariance[14] = input_covariance + 0.20

        # Rotate data TODO: Rotate covariances. Take a look at motion transformations file
        rotated_vel = R * vel

        # Store velocities
        if self.imu_init:
            w = array([self.imu_data.angular_velocity.x,self.imu_data.angular_velocity.y,self.imu_data.angular_velocity.z])
            aux = array([rotated_vel[0],rotated_vel[1],rotated_vel[2]]) - cross(w, trans)
            msg.twist.twist.linear.x = aux[0]
            msg.twist.twist.linear.y = aux[1]
            msg.twist.twist.linear.z = aux[2]
        else:
            msg.twist.twist.linear.x = rotated_vel[0]
            msg.twist.twist.linear.y = rotated_vel[1]
            msg.twist.twist.linear.z = rotated_vel[2]

        # Compute altitude
        altitude_beams = []
        for beam in range(4):
            if dvl.altitudeBeam[beam] > 0.0 and dvl.dataGood[beam] == 1:
                altitude_beams.append(dvl.altitudeBeam[beam])
        if len(altitude_beams) > 1:
            altitude_beams.sort()
            self.altitude = altitude_beams[1]
            self.bad_altitude_counter = 0
        elif len(altitude_beams) == 1:
            self.altitude = altitude_beams[0]
            self.bad_altitude_counter = 0
        else:
            self.bad_altitude_counter += 1
            if self.bad_altitude_counter < 5:
                rospy.loginfo('%s: invalid DVL altitude for a short time. Keeping last known altitude', self.name)
            else:
                rospy.loginfo('%s: invalid DVL altitude for a long time. Computing mean', self.name)
                self.altitude = (dvl.altitudeBeam[0] + dvl.altitudeBeam[1] + dvl.altitudeBeam[2] + dvl.altitudeBeam[3]) / 4.0
        # Do a velocity update
        self.pub_vel.publish(msg)


    def update_rdi_DVL(self, dvl):
        """ This method is the callback of the rdi DVL """
        # Store dvl_last_update var, used by check_sensors method
        self.dvl_last_update = rospy.Time.now()

        # Check dvl provenance
        if not dvl.header.frame_id in self.rdi_frames:
            self.rdi_frames.append(dvl.header.frame_id)
            rospy.loginfo('%s: using RDI DVL data from %s', self.name, dvl.header.frame_id)
        if len(self.rdi_frames) > 1:
            rospy.logwarn('%s: RDI DVL data comes from more than one place', self.name)
        if len(self.linkquest_frames) > 0:
            rospy.loginfo('%s: using data from both Linkquest and RDI DVL', self.name)

        # DVL update
        # If dvl_update == 0 --> No update
        # If dvl_update == 1 --> Update wrt bottom
        # If dvl_update == 2 --> Update wrt water
        dvl_update = 0

        #rospy.logfatal('%s: bi_x_axis: %s', self.name, dvl.bi_x_axis)
        #rospy.logfatal('%s: bi_y_axis: %s', self.name, dvl.bi_y_axis)
        #rospy.logfatal('%s: bi_z_axis: %s', self.name, dvl.bi_z_axis)

        if dvl.bi_status == "A": # and dvl.bi_error > -32.0:
            if (abs(dvl.bi_x_axis) < self.dvl_max_v and
                abs(dvl.bi_y_axis) < self.dvl_max_v and
                abs(dvl.bi_z_axis) < self.dvl_max_v):
                v = PyKDL.Vector(dvl.bi_x_axis, dvl.bi_y_axis, dvl.bi_z_axis)
                dvl_update = 1
                self.diagnostic.add("DVL_data", "bottom")
                rospy.loginfo("%s: bottom DVL, not checking bi_error", self.name)
        elif dvl.wi_status == "A" and dvl.wi_error > -32.0:
            if (abs(dvl.wi_x_axis) < self.dvl_max_v and
                abs(dvl.wi_y_axis) < self.dvl_max_v and
                abs(dvl.wi_z_axis) < self.dvl_max_v):
                v = PyKDL.Vector(dvl.wi_x_axis, dvl.wi_y_axis, dvl.wi_z_axis)
                dvl_update = 0  # NO WATER DVL
                self.diagnostic.add("DVL_data", "water")

        # Filter to check if the altitude is reliable
        if dvl.bi_status == "A" and dvl.bi_error > -32.0:
            self.bottom_status =  self.bottom_status + 1
        else:
            self.bottom_status = 0

        if self.bottom_status > 4:
            self.altitude = dvl.bd_range
            self.diagnostic.add("Altitude", str(self.altitude))
        else:
            self.altitude = INVALID_ALTITUDE
            self.diagnostic.add("Altitude", "Invalid altitude")


        if dvl_update != 0:
            self.dvl_init = True

            msg = TwistWithCovarianceStamped()
            msg.header.stamp = dvl.header.stamp
            msg.header.frame_id = "sparus2"  # THIS MEANS THE CONVERSION IS HERE!

            # Rotation
            R = PyKDL.Rotation.RPY(math.pi, 0.0, math.pi * 3.0 / 4.0)
            v = R * v  # TODO: Rotate the DVL properly. Config file. Rotate covariances. Take a look at motion transformations file
            if not(abs(v[0]) < self.dvl_max_v and abs(v[1]) < self.dvl_max_v/10.0):
                #rospy.logwarn('DVL surge or sway velocities to high')
                #return 0
                pass

            # Trans
            trans = array([-0.41327, 0.0, 0.09229])
            w = array([self.imu_data.angular_velocity.x, self.imu_data.angular_velocity.y, self.imu_data.angular_velocity.z])
            aux = array([v[0], v[1], v[2]]) - cross(w, trans)

            # Create a new velocity
            msg.twist.twist.linear.x = aux[0]
            msg.twist.twist.linear.y = aux[1]
            msg.twist.twist.linear.z = aux[2]

            #rospy.logfatal('%s: velocity x: %s', self.name, msg.twist.twist.linear.x)
            #rospy.logfatal('%s: velocity y: %s', self.name, msg.twist.twist.linear.y)
            #rospy.logfatal('%s: velocity z: %s', self.name, msg.twist.twist.linear.z)

            # Only the number in the covariance matrix diagonal
            # are used for the updates!
            if (dvl_update == 1) and (self.nav.position.depth > 1.5):
                msg.twist.covariance[0] = 0.002  # TODO: Config file
                msg.twist.covariance[7] = 0.002
                msg.twist.covariance[14] = 0.002
            else:
                msg.twist.covariance[0] = 0.02  # TODO: Config file
                msg.twist.covariance[7] = 0.02
                msg.twist.covariance[14] = 0.02

            self.pub_vel.publish(msg)


    def update_IMU(self, imu):  # TODO: everything with quaternions, if possible. Convert covariances
        """ This method is the callback of the IMU """
        # Store last_imu_update stamp. This is used by the check_sensors method
        self.imu_last_update = imu.header.stamp

        # Set navigator flag to true
        self.imu_data = imu
        self.imu_init = True

        # Publish imu message
        imu.header.frame_id = self.robot_frame_name
        self.pub_imu.publish(imu)


    def no_disable_thrusters_message(self, event):
        """ Timer to show an error in disable thrusters service """
        rospy.logfatal('%s: error creating client to disable thrusters', self.name)


    def no_recovery_actions_message(self, event):
        """ Timer to show an error in recovery actions service """
        rospy.logfatal('%s: error creating client to recovery actions', self.name)


    def get_config(self):
        """ Get config from param server """
        param_dict = {'robot_frame_name': 'navigator/robot_frame_name',
                      'world_frame_name': 'navigator/world_frame_name',
                      'dvl_max_v': 'navigator/dvl_max_v',
                      'depth_correction': 'navigator/depth_correction',
                      'water_density': 'navigator/water_density',
                      'gps_update': 'navigator/gps_update',
                      'gps_init_samples': 'navigator/gps_init_samples',
                      'check_sensors_period': 'navigator/check_sensors_period',
                      'dvl_max_period_error': 'navigator/dvl_max_period_error',
                      'imu_max_period_error': 'navigator/imu_max_period_error',
                      'max_init_time': 'navigator/max_init_time',
                      'fixed_ned': 'navigator/fixed_ned',
                      'ned_lat': 'navigator/ned_latitude',
                      'ned_lon': 'navigator/ned_longitude',
                      'only_one_gps_update': 'navigator/only_one_gps_update'}

        if not cola2_ros_lib.getRosParams(self, param_dict, self.name):
            rospy.logfatal("%s: thrusters disabled due to invalid config parameters!", self.name)
            try:
                self.abort_thrusters_srv(EmptyRequest())
            except rospy.exceptions.ROSException:
                rospy.logfatal('%s: error disabling thrusters', self.name)
                rospy.signal_shutdown('Error calling disable thrusters servicet')

        self.bar_to_meters_depth = 100000.0 / (self.water_density * 9.81)


if __name__ == '__main__':
    try:
        #   Init node
        rospy.init_node('navigator_s2')
        navigator_s2 = NavigatorS2(rospy.get_name())
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
