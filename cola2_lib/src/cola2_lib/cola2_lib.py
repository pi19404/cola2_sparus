#!/usr/bin/env python

#import pylab
from math import floor, pi, sqrt
from numpy import zeros, ones, array
import NED


class PID:
    def __init__(self, p, i, d, fff, time_in_sec, i_limit = None):
        if i_limit == None:
            i_limit = ones(len(p))
        if len(p) == len(i) and len(p) == len(d) and len(p) == len(fff) and len(p) == len(i_limit):
            self.kp = p
            self.ti = i
            self.td = d
            self.fff = fff
            self.n = len(p)
            self.ek_1 = zeros(self.n)
            self.eik_1 = zeros(self.n)
            self.past_time = time_in_sec
            self.i_limit = i_limit
        else:
            print 'ERROR: Bad vectors size!!'


    def compute(self, desired, current, time_in_sec):
        # Note: All the operations are done element by element
        T = time_in_sec - self.past_time
        if T < 0.001:
            T = 0.001
        self.past_time = time_in_sec

        # Compute errors
        if  len(desired) == self.n and len(current) == self.n:
            ek = zeros(self.n)
            eik = zeros(self.n)
            edotk = zeros(self.n)

            ek = desired - current
            edotk = (ek - self.ek_1) / T
            eik = self.eik_1 + (ek * T)

            # Control law
            tau = zeros(self.n)
            for i in range(self.n):
                # Compute the integral part if ti > 0
                if self.ti[i] > 0.0:
                    # Integral part
                    integral_part = (self.kp[i]/self.ti[i])*eik[i]

                    # Saturate integral part (anti-windup condition, integral part not higher than a value)
                    integral_part = saturateValueFloat(integral_part, self.i_limit[i])

                    # Restore eik
                    if self.kp[i] > 0:
                        eik[i] = integral_part * self.ti[i] / self.kp[i]
                    else:
                        eik[i] = 0

                    # Compute tau
                    tau[i] = self.kp[i]*ek[i] + self.kp[i]*self.td[i]*edotk[i] + integral_part + self.fff[i]
                else:
                    # Compute tau without integral part
                    tau[i] = self.kp[i]*ek[i] + self.kp[i]*self.td[i]*edotk[i] + self.fff[i]

            # Saturate tau
            tau = saturateValue(tau, 1.0)

            self.ek_1 = ek
            self.eik_1 = eik
            return tau
        else:
            return None


    def computeWithoutSaturation(self, desired, current, time_in_sec):
        # Note: All the operations are done element by element
        T = time_in_sec - self.past_time
        if T == 0.0:
            T = 0.001
        self.past_time = time_in_sec

        # Compute errors
        if  len(desired) == self.n and len(current) == self.n:
            ek = zeros(self.n)
            eik = zeros(self.n)
            edotk = zeros(self.n)

            ek = desired - current
            edotk = (ek - self.ek_1) / T
            eik = self.eik_1 + (ek * T)

            # Control law
            tau = zeros(self.n)
            for i in range(self.n):
                # Compute the integral part if ti > 0
                if self.ti[i] > 0.0:
                    integral_part = (self.kp[i]/self.ti[i])*eik[i]

                    # Compute tau
                    tau[i] = self.kp[i]*ek[i] + self.kp[i]*self.td[i]*edotk[i] + integral_part + self.fff[i]

                    # Anti-windup condition: if tau is saturated and ek has the
                    # same sign than tau, eik does not increment
                    if abs(tau[i]) > 1.0 and ek[i]*tau[i] > 0:
                        eik[i] = self.eik_1[i]
                        # Because eik has been modified, recompute tau
                        integral_part = (self.kp[i]/self.ti[i])*eik[i]
                        tau[i] = self.kp[i]*ek[i] + self.kp[i]*self.td[i]*edotk[i] + integral_part + self.fff[i]
                else:
                    # Compute tau without integral part
                    tau[i] = self.kp[i]*ek[i] + self.kp[i]*self.td[i]*edotk[i] + self.fff[i]

            self.ek_1 = ek
            self.eik_1 = eik
            return tau

        else:
            return None


    def reset(self, now = None):
        self.ek_1 = zeros(self.n)
        self.eik_1 = zeros(self.n)
        if now != None:
            self.past_time = now


    def resetDof(self, dof, now = None):
        self.ek_1[dof] = 0
        self.eik_1[dof] = 0
        if now != None:
            self.past_time = now


class Trajectory:
    def __init__(self):
        self.loaded = False
        self.init_ned = False

    def load(self, trajectory_type, lat_or_north, lon_or_east, depth, altitude, altitude_mode, mode, actions,
                 roll, pitch, yaw, wait, disable_axis, tolerance, priority):

        assert(len(lat_or_north) == len(lon_or_east) == len(depth) == len(altitude) ==
               len(altitude_mode) == len(mode) == len(actions) == len(roll) ==
               len(pitch) == len(yaw) == len(wait))

        self.trajectory_type = trajectory_type
        if (trajectory_type == 'absolute'):
            self.lat = lat_or_north
            self.lon = lon_or_east
        elif (trajectory_type == 'relative'):
            self.north = lat_or_north
            self.east = lon_or_east
        self.depth = depth
        self.altitude = altitude
        self.altitude_mode = altitude_mode
        self.mode = mode
        self.actions = actions
        self.roll = roll
        self.pitch = pitch
        self.yaw = yaw
        self.wait = wait
        self.disable_axis = disable_axis
        self.tolerance = tolerance
        self.priority = priority

        self.loaded = True


    def initNed(self, lat, lon):
        self.ned = NED.NED(lat, lon, 0.0)
        self.init_ned = True
        self.lat_origin = lat
        self.lon_origin = lon


    def getWaypointNed(self, i):
        assert self.init_ned, '[getWaypointNed] First initialize NED system'
        assert self.loaded, '[getWaypointNed] First load a trajectory'
        assert i < len(self.depth), '[getWaypointNed] Waypoint out of range'
        if (self.trajectory_type == 'absolute'):
            aux = self.ned.geodetic2ned([self.lat[i], self.lon[i], 0.0])
            aux[NED.DEPTH] = self.depth[i]
        elif (self.trajectory_type == 'relative'):
            aux = array([self.north[i], self.east[i], self.depth[i]])
        return aux


    def computeWaypointTimeout(self, i, current_north, current_east,
                               current_depth, current_altitude, max_vel = 0.4):
        wp = self.getWaypointNed(i)
        if self.altitude_mode[i]:
            distance = sqrt((wp[NED.NORTH] - current_north)**2 +
                            (wp[NED.EAST] - current_east)**2 +
                            (current_altitude - wp[NED.DEPTH])**2)
        else:
            distance = sqrt((wp[NED.NORTH] - current_north)**2 +
                            (wp[NED.EAST] - current_east)**2 +
                            (wp[NED.DEPTH] - current_depth)**2)

        time = (distance / max_vel) * 1.5 + 30
        return time


class ErrorCode:
    def __init__(self):
        pass

    INIT = 15
    BAT_WARNING = 14
    BAT_ERROR = 13
    NAV_STS_WARNING = 12
    NAV_STS_ERROR = 11
    INTERNAL_SENSORS_WARNING = 10
    INTERNAL_SENSORS_ERROR = 9
    DVL_BOTTOM_FAIL = 8
    CURRENT_WAYPOINT_BASE = 6 # to 1


def saturateVector(v, min_max) :
    ret = zeros( len(v) )
    for i in range( len(v) ) :
        if v[i] < -min_max[i] : ret[i] = -min_max[i]
        elif v[i] > min_max[i] : ret[i] = min_max[i]
        else : ret[i] = v[i]
    return ret


def saturateValue(v, min_max) :
    ret = zeros( len(v) )
    for i in range( len(v) ) :
        if v[i] < -min_max : ret[i] = -min_max
        elif v[i] > min_max : ret[i] = min_max
        else : ret[i] = v[i]
    return ret


def saturateValueFloat(v, min_max):
    if v > min_max:
        v = min_max
    elif v < -min_max:
        v = -min_max

    return v

#def computePid6Dof(desired, current, kp, ki, kd, sat, ek_1, eik_1, T):
#    ek = zeros(6)
#    eik = zeros(6)
#    edotk = zeros(6)
#
#    # All the operations are done element by element
#    ek = desired - current
#    edotk = (ek - ek_1) / T
#    eik = eik_1 + (ek * T)
#    eik = saturateVector(eik, sat)
#    #print "ek: \n" + str(ek)
#    #print "eik: \n" + str(eik)
#    #print "edotk: \n" + str(edotk)
#    #print "ek_1: \n" + str(ek_1)
#    #print "eik_1: \n" + str(eik_1)
#
#    # Control law
#    tau = zeros(6)
#    tau = kp * ek + ki * eik + kd * edotk
#    tau = saturateValue(tau, 1.0)
#
#    return [tau, ek, eik]
#
#
#def computePid6Dofv2(desired, current, kp, ti, td, fff, ek_1, eik_1, T):
#    #Note: All the operations are done element by element
#    #Compute errors
#    ek = zeros(6)
#    eik = zeros(6)
#    edotk = zeros(6)
#
#    ek = desired - current
#    edotk = (ek - ek_1) / T
#    eik = eik_1 + (ek * T)
#
#    # Control law
#    tau = zeros(6)
#    for i in range(6):
#        # Compute the integral part if ti > 0
#        if ti[i] > 0.0:
#            integral_part = (kp[i]/ti[i])*eik[i]
#
#            #Compute tau
#            tau[i] = kp[i]*ek[i] + kp[i]*td[i]*edotk[i] + integral_part + fff[i]
#
#            #Anti-windup condition: if tau is saturated and ek has the same sign than tau, eik does not increment
#            if abs(tau[i]) > 1.0 and ek[i]*tau[i] > 0:
#                eik[i] = eik_1[i]
#
#                #because eik has been modified, recompute tau
#                integral_part = (kp[i]/ti[i])*eik[i]
#                tau[i] = kp[i]*ek[i] + kp[i]*td[i]*edotk[i] + integral_part + fff[i]
#        else:
#            #Compute tau without integral part
#            tau[i] = kp[i]*ek[i] + kp[i]*td[i]*edotk[i] + fff[i]
#
#    #Saturate tau
#    tau = saturateValue(tau, 1.0)
#
#    return [tau, ek, eik, kp[0]*ek[0], (kp[0]/ti[0])*eik[0]]


def normalizeAngle(angle):
    return wrapAngle(angle)

def wrapAngle(angle):  # Should be the default option, "wrap angle" is the proper term in English
    return (angle + ( 2.0 * pi * floor( ( pi - angle ) / ( 2.0 * pi ) ) ) )

def slopeFilter(max_slope, v, v1):
    dy = v - v1
    if dy > max_slope:
        return (v1 + max_slope)
    elif dy < -max_slope:
        return (v1 - max_slope)
    return v


def polyval(poly, f) :
    # TODO: To be matbal compatible. Make it more efficient!
    p = list(poly)
    p.reverse()

    value = 0.0
    change_sign = False
    ret = 0

    if f < 0.0:
        value = abs(f)
        change_sign = True
    else :
        value = f
        change_sign = False

    for i in range(len(p)) :
        ret = ret + pow(value, i) * p[i]

    if change_sign:
        ret = ret * -1.0

    return ret


def test() :
    print "SaturateVector:"
    print str(saturateVector([1.8,0.3,-3.2, -0.7], ones(4)))

    print "SaturateValue:"
    print str(saturateValue([1.8,0.3,-3.2, -0.7], 1.0))

    print "Normalize angle 7.14 = " + str(normalizeAngle(7.14))

if __name__ == '__main__':
    test()
