#!/usr/bin/env python

"""
Created on 02/13/2014

@author: Narcis Palomeras
"""

# ROS imports
import roslib
roslib.load_manifest('cola2_safety')
import rospy
import numpy as np

from auv_msgs.msg import WorldWaypointReq
from auv_msgs.msg import GoalDescriptor
from auv_msgs.msg import NavSts
from cola2_lib import cola2_ros_lib


class VirtualCage(object):
    """ @@This class prevents an AUV to move beyond a virtual limits.@@"""

    def __init__(self, name):
        """ Init the class. """
        self.name = name

        # Get config parameters
        self.north_limit = 10.0
        self.east_limit = 10.0
        self.rotation_angle = 0.0
        self.get_config()
        self.virtual_cage_enabled = False
        self.navigation_enabled = False
        
        # create rotation matrix
        self.rotation_matrix = np.array([np.cos(self.rotation_angle), 
                                         -np.sin(self.rotation_angle),
                                         np.sin(self.rotation_angle), 
                                         np.cos(self.rotation_angle)])
        self.rotation_matrix = self.rotation_matrix.reshape(2, 2)
        
        # Request
        self.request = WorldWaypointReq()
        self.request.header.frame_id = '/world'
        self.request.goal.requester = self.name
        self.request.goal.priority = GoalDescriptor.PRIORITY_EMERGENCY
        self.request.position.north = 0.0
        self.request.position.east = 0.0
        self.request.disable_axis.x = False
        self.request.disable_axis.y = False
        self.request.disable_axis.z = True
        self.request.disable_axis.roll = True
        self.request.disable_axis.pitch = True
        self.request.disable_axis.yaw = False
        
        # Publisher
        self.pub_world_waypoint_req = rospy.Publisher(
            "/cola2_control/world_waypoint_req", WorldWaypointReq)

        # Subscriber
        rospy.Subscriber("/cola2_navigation/nav_sts",
                         NavSts,
                         self.update_nav_sts)
                         
        # Timer
        rospy.Timer(rospy.Duration(1.0), self.show_error)


    def show_error(self, event):
        """ Show an error if the vehicle starts out of the cage. """
        if self.navigation_enabled and not self.virtual_cage_enabled:
            rospy.logerr("%s: Vehicle out of the cage!", self.name)
            
            
    def update_nav_sts(self, nav):
        """ When a new navigation is received check if the vehicle is still
            inside the cage. If it is not as it to move to (0,0)
            with the current yaw."""
        
        self.navigation_enabled = True
        vehicle_position = np.array([nav.position.north,
                                     nav.position.east])
                                     
        rotated_position = np.dot(self.rotation_matrix, vehicle_position)

        if (abs(rotated_position[0]) > self.north_limit or
            abs(rotated_position[1]) > self.east_limit):
            # Out of the cage!
            self.request.header.stamp = rospy.Time.now()
            self.request.orientation.yaw = nav.orientation.yaw
            self.pub_world_waypoint_req.publish(self.request)
        else:
            # Inside the cage
            self.virtual_cage_enabled = True
            

    def get_config(self):
        """ Read parameters from ROS Param Server."""
        param_dict = {'north_limit': 'virtual_cage/north_limit',
                      'east_limit': 'virtual_cage/east_limit',
                      'rotation_angle': 'virtual_cage/rotation_angle'}

        cola2_ros_lib.getRosParams(self, param_dict, self.name)


if __name__ == '__main__':
    try:
        rospy.init_node('virtual_cage')
        V_CAGE = VirtualCage(rospy.get_name())
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
