#!/usr/bin/env python
"""@@This node check AUV depth and altitude, and is mainly used to avoid collisions.@@"""

"""
Created on Fri Mar 22 2013

@author: narcis palomeras
"""

# ROS imports
import roslib
roslib.load_manifest('cola2_safety')
import rospy

from auv_msgs.msg import BodyVelocityReq
from auv_msgs.msg import GoalDescriptor
from auv_msgs.msg import NavSts
from cola2_lib import cola2_ros_lib


class SafeDepthAltitude(object):
    """ This node is able to check altitude and depth """

    def __init__(self, name):
        """ Init the class """
        # Init class vars
        self.name = name

        # Get config parameters
        self.max_depth = 1.0
        self.min_altitude = 5.0
        self.get_config()

        # Publisher
        self.pub_body_velocity_req = rospy.Publisher(
            "/cola2_control/body_velocity_req", BodyVelocityReq)

        # Subscriber
        rospy.Subscriber("/cola2_navigation/nav_sts",
                         NavSts,
                         self.update_nav_sts)

        # Show message
        rospy.loginfo("%s: initialized", self.name)


    def update_nav_sts(self, nav):
        """ This is the callback of the navigation, but it is used as
            the main method """
        if ((nav.altitude > 0 and nav.altitude < self.min_altitude and nav.position.depth > 0.5) or
            (nav.position.depth > self.max_depth)):
            # Show message
            if (nav.altitude > 0 and nav.altitude < self.min_altitude and nav.position.depth > 0.5):
                rospy.logwarn("%s: invalid altitude: %s", self.name, nav.altitude)
            if (nav.position.depth > self.max_depth):
                rospy.logwarn("%s: invalid depth: %s", self.name, nav.position.depth)

            # Go up
            bvr = BodyVelocityReq()
            bvr.twist.linear.x = 0.0
            bvr.twist.linear.y = 0.0
            bvr.twist.linear.z = -0.5
            bvr.twist.angular.x = 0.0
            bvr.twist.angular.y = 0.0
            bvr.twist.angular.z = 0.0
            bvr.disable_axis.x = True
            bvr.disable_axis.y = True
            bvr.disable_axis.z = False
            bvr.disable_axis.roll = True
            bvr.disable_axis.pitch = True
            bvr.disable_axis.yaw = False
            bvr.goal.priority =  GoalDescriptor.PRIORITY_MANUAL_OVERRIDE + 1
            bvr.goal.requester = self.name
            bvr.header.stamp = rospy.Time.now()
            self.pub_body_velocity_req.publish(bvr)


    def get_config(self):
        param_dict = {'max_depth': 'safety_safe_depth_altitude/max_depth',
                      'min_altitude': 'safety_safe_depth_altitude/min_altitude'}

        if not cola2_ros_lib.getRosParams(self, param_dict, self.name):
            self.bad_config_timer = rospy.Timer(rospy.Duration(0.4), self.bad_config_message)


    def bad_config_message(self, event):
        """ Timer to show an error if loading parameters failed """
        rospy.logerr('%s: bad parameters in param server!', self.name)


if __name__ == '__main__':
    try:
        rospy.init_node('safe_depth_altitude')
        safe_depth_altitude = SafeDepthAltitude(rospy.get_name())
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
