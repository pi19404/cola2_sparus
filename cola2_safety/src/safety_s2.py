#!/usr/bin/env python
"""@@Safety node is used to check an absolute timeout. It also checks if navigator
is publishing and if battery voltage is ok.@@"""

# ROS imports
import roslib
roslib.load_manifest('cola2_safety')
roslib.load_manifest('cola2_control')
import rospy
import rosparam

from std_msgs.msg import String
from auv_msgs.msg import BodyVelocityReq
from auv_msgs.msg import GoalDescriptor
from std_srvs.srv import Empty, EmptyRequest, EmptyResponse
from cola2_safety.msg import TotalTime, EMUSBMS
from cola2_control.msg import Setpoints
from auv_msgs.msg import NavSts

from cola2_lib import cola2_ros_lib

from numpy import array


class SafetyS2(object):
    """ This node is used to check some generic safety rules """

    def __init__(self, name):
        """ Constructor """
        self.name = name

        # Init class vars
        self.trap_counter = 0
        self.trapped = False

        # Initial time
        self.init_time = rospy.Time.now()

        # Get config parameters
        self.emerge = False
        self.absolute_timeout = 20
        self.min_cell_voltage = 3.4
        self.get_config()

        # Messages
        self.nav = NavSts()
        self.bvr = BodyVelocityReq()

        # Publisher
        self.pub_total_time = rospy.Publisher(
             "/cola2_safety/total_time", TotalTime)
        self.pub_thrusters = rospy.Publisher(
             "/cola2_control/thrusters_data", Setpoints)

        # Disable thrusters service
        rospy.loginfo("%s: waiting for services", self.name)
        try:
            rospy.wait_for_service(
                     '/cola2_control/disable_thrusters', 20)
            self.abort_thrusters_srv = rospy.ServiceProxy(
                     '/cola2_control/disable_thrusters', Empty)
        except rospy.exceptions.ROSException:
            self.no_disable_thrusters_service_timer = rospy.Timer(rospy.Duration(0.4), self.no_disable_thrusters_message)

        # Create reset timeout service
        self.reset_timeout_srv = rospy.Service('/cola2_safety/reset_timeout',
                                        Empty,
                                        self.reset_timeout)

        # Timer to check absolute timeout and publish config
        self.published_config = False
        rospy.Timer(rospy.Duration(0.1), self.check_absolute_timeout_and_publish_config)  # Timer period must be 0.1 sec

        # Check navigator
        self.last_navigator_callback = rospy.Time.now()
        self.init_navigator_check = False
        rospy.Timer(rospy.Duration(0.1), self.navigator_check)  # Timer period must be 0.1 sec
        rospy.Subscriber("/cola2_navigation/nav_sts", NavSts, self.update_nav_sts)

        # Check battery
        self.last_bms_callback = rospy.Time.now()
        self.low_battery = False
        self.init_bms_check = False
        rospy.Timer(rospy.Duration(0.1), self.battery_check)  # Timer period must be 0.1 sec
        rospy.Subscriber("/cola2_safety/emus_bms", EMUSBMS, self.update_bms)

        # Subscriber for the trap avoidance
        rospy.Subscriber("/cola2_control/merged_body_velocity_req",
                         BodyVelocityReq,
                         self.update_bvr)
        # rospy.Timer(rospy.Duration(0.1), self.trap_avoidance)  # Timer period must be 0.1 sec
        rospy.loginfo("%s: trap avoidance disabled", self.name)

        # Show message
        rospy.loginfo("%s: initialized", self.name)


    def check_absolute_timeout_and_publish_config(self, event):
        """ This is the callback of the main timer """
        dt = (rospy.Time.now() - self.init_time).to_sec()
        if dt > 0.0:
            # Check absolute timeout
            if dt > self.absolute_timeout:
                self.abort_thrusters_srv(EmptyRequest())
                if self.emerge:
                    self.emerge_routine()
                    rospy.logwarn("%s: absolute timeout reached! EMERGE", self.name)
                else:
                    rospy.logwarn("%s: absolute timeout reached! THRUSTERS OFF", self.name)

            # Publish total time
            msg = TotalTime()
            msg.total_time = int(dt)
            msg.timeout = int(self.absolute_timeout)
            self.pub_total_time.publish(msg)

        # Publish config stored in rosparam server
        if dt > 30.0 and not self.published_config:
            self.published_config = True
            pub = rospy.Publisher("/cola2_safety/configuration", String)
            msg = String()
            parameters_list = rosparam.list_params('/')
            data = '\n'
            for parameter in parameters_list:
                data = data + 'Parameter: ' + parameter + '\n'
                data = data + 'Value: ' + str(rospy.get_param(parameter)) + '\n\n'
            msg.data = data
            pub.publish(msg)


    def reset_timeout(self, req):
        """ Service used to reset timeout """
        self.absolute_timeout = self.absolute_timeout + (rospy.Time.now() - self.init_time).to_sec()
        return EmptyResponse()


    def update_nav_sts(self, data):
        """ Navigator callback """
        self.nav = data
        self.last_navigator_callback = rospy.Time.now()
        if not self.init_navigator_check:
            self.init_navigator_check = True


    def update_bvr(self, data):
        """ Navigator callback """
        self.bvr = data


    def navigator_check(self, event):
        """ This is a timer to check if navigator is publishing """
        dt = (rospy.Time.now() - self.last_navigator_callback).to_sec()
        if self.init_navigator_check or dt > 40.0:  # Give some time to initialize
            if dt > 5.0:
                self.abort_thrusters_srv(EmptyRequest())
                if self.emerge:
                    self.emerge_routine()
                    rospy.logwarn("%s: navigator is not publishing! EMERGE", self.name)
                else:
                    rospy.logwarn("%s: navigator is not publishing! THRUSTERS OFF", self.name)


    def update_bms(self, data):
        """ This is the callback of the bms """
        self.last_bms_callback = rospy.Time.now()
        if not self.init_bms_check:
            self.init_bms_check = True

        if data.minCellVoltage < self.min_cell_voltage:
            self.low_battery = True


    def battery_check(self, event):
        """ Timer to check battery """
        dt = (rospy.Time.now() - self.last_bms_callback).to_sec()
        if self.init_bms_check or dt > 20:
            if dt > 5.0:
                self.abort_thrusters_srv(EmptyRequest())
                if self.emerge:
                    self.emerge_routine()
                    rospy.logwarn("%s: bms is not publishing! EMERGE", self.name)
                else:
                    rospy.logwarn("%s: bms is not publishing! THRUSTERS OFF", self.name)
        if self.low_battery:
            self.abort_thrusters_srv(EmptyRequest())
            if self.emerge:
                self.emerge_routine()
                rospy.logwarn("%s: low battery! EMERGE", self.name)
            else:
                rospy.logwarn("%s: low battery! THRUSTERS OFF", self.name)


    def trap_avoidance(self, event):
        # Only if the vehicle is moving forward
        if self.bvr.twist.linear.x > 0.01:
            # If current velocity is below 1/3 of desired velocity
            if self.nav.body_velocity.x < self.bvr.twist.linear.x/3.0:
                self.trap_counter = self.trap_counter + 1
            else:
                self.trap_counter = 0
        else:
            self.trap_counter = 0

        # ...for more than 20 seconds
        if self.trap_counter > 200:
            self.abort_thrusters_srv(EmptyRequest())
            self.trapped = True

        if self.trapped:
            rospy.logfatal('%s: vehicle trapped!', self.name)
            if self.emerge:
                self.emerge_routine()


    def emerge_routine(self):
        """ Emerge routine """
        thrusters = Setpoints()
        thrusters.header.frame_id = 'sparus2'
        thrusters.header.stamp = rospy.Time.now()
        thrusters.setpoints = [-0.75, 0.0, 0.0]  # Be careful here!
        self.pub_thrusters.publish(thrusters)


    def get_config(self):
        """ Get config from param server """
        param_dict = {'absolute_timeout': 'safety_s2/absolute_timeout',
                      'emerge': 'safety_s2/emerge',
                      'min_cell_voltage': 'safety_s2/min_cell_voltage'}

        if not cola2_ros_lib.getRosParams(self, param_dict, self.name):
            self.bad_config_timer = rospy.Timer(rospy.Duration(0.4), self.bad_config_message)


    def bad_config_message(self, event):
        """ Timer to show an error if loading parameters failed """
        rospy.logfatal('%s: bad parameters in param server!', self.name)


    def no_disable_thrusters_message(self, event):
        """ Timer to show an error in disable thrusters service """
        rospy.logfatal('%s: error creating client to disable thrusters', self.name)


if __name__ == '__main__':
    try:
        rospy.init_node('safety_s2')
        safety_s2 = SafetyS2(rospy.get_name())
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
