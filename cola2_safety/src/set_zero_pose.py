#!/usr/bin/env python
"""@@If teleoperation is lost for more than 5 seconds, this node tells the robot
to surface.@@"""

# ROS imports
import roslib
roslib.load_manifest('cola2_safety')
import rospy

from std_msgs.msg import String
from auv_msgs.msg import WorldWaypointReq
from auv_msgs.msg import GoalDescriptor


class SetZeroPose(object):
    """ Send zero pose on depth and yaw with low + 2 priority
        when there is no teleoperation """

    def __init__(self, name):
        """ Initialize the class """
        # Init class vars
        self.name = name

        # Initial time
        self.last_time = rospy.Time.now()
        self.initial_time = rospy.Time.now()
        self.initialization_time = 15.0  # Wait some time when node starts

        # Publisher
        self.pub_world_waypoint_req = rospy.Publisher(
             "/cola2_control/world_waypoint_req", WorldWaypointReq)

        # Subscriber
        rospy.Subscriber("cola2_control/map_ack_ack",
                         String, self.map_ack_ack_callback)

        # Timer
        rospy.Timer(rospy.Duration(0.1), self.set_zero_pose)

        # Show message
        rospy.loginfo("%s: initialized", self.name)


    def set_zero_pose(self, event):
        """ Go up and point to the north """
        if (rospy.Time.now() - self.initial_time).to_sec() > self.initialization_time:
            dt = (rospy.Time.now() - self.last_time).to_sec()
            if dt > 5.0:  # If 5 seconds without teleoperation
                rospy.loginfo("%s: active, 5 seconds without teleoperation", self.name)
                wwr = WorldWaypointReq()
                wwr.goal.requester = 'set_zero_pose'
                wwr.goal.priority = GoalDescriptor.PRIORITY_LOW + 2
                wwr.header.stamp = rospy.Time.now()
                wwr.altitude_mode = False
                wwr.position.north = 0.0
                wwr.position.east = 0.0
                wwr.position.depth = 0.0
                wwr.altitude = 1000
                wwr.orientation.roll = 0.0
                wwr.orientation.pitch = 0.0
                wwr.orientation.yaw = 0.0
                wwr.disable_axis.x = True
                wwr.disable_axis.y = True
                wwr.disable_axis.z = False
                wwr.disable_axis.roll = True
                wwr.disable_axis.pitch = True
                wwr.disable_axis.yaw = False
                self.pub_world_waypoint_req.publish(wwr)


    def map_ack_ack_callback(self, data):
        """ Callback to test comunication between map_ack and teleoperation """
        self.last_time = rospy.Time.now()


if __name__ == '__main__':
    try:
        rospy.init_node('set_zero_pose')
        __set_zero_pose__ = SetZeroPose(rospy.get_name())
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
