# -*- coding: utf-8 -*-
"""
Created on Sat Nov  9 23:32:56 2013

@author: narcis
"""
import rosmsg
from link_html import LinkHTML
import rid_graphics as graph

from jinja2 import Environment, FileSystemLoader

def __index_page__(description, page_info):
    return


def __node_page__(node, subcribed_to_topic, publishes_topic, page_info):
    env = Environment(loader=FileSystemLoader('rid/templates'))
    template = env.get_template('node.tex')

    publish_html = node.get_publish_html()
    # fill depences with subcribed_to_topic
    if publish_html is not None:
        for i in publish_html:
            elements = subcribed_to_topic[i.topic]
            i.dependences = list()
            for j in elements:
                i.dependences.append(LinkHTML(j, None, j[1:] + '.html'))
            if len(i.dependences) == 0:
                i.dependences = None

    subscribe_html = node.get_subscribe_html()
    # fill depences with publishes_topic
    if subscribe_html is not None:
        for i in subscribe_html:
            elements = publishes_topic[i.topic]
            i.dependences = list()
            for j in elements:
                i.dependences.append(LinkHTML(j, None, j[1:] + '.html'))
            if len(i.dependences) == 0:
                i.dependences = None

    # Create node image
    graph.__create_node_graph__(node.get_name(),
                                publish_html,
                                subscribe_html)

    output_from_parsed_template = template.render(
            node_name = node.get_name(),
            description = node.get_description().replace('\n', ' '),
            config_file = node.get_config_file_html(),
            publish = publish_html,
            subscribe = subscribe_html,
            service = node.get_service_html(),
            actionlib = node.get_actionlib_html(),
            nodes_list = page_info['nodes_list'],
            config_files_list = page_info['config_files_list'],
            topics_list = page_info['topics_list'],
            services_list = page_info['services_list'],
            date = page_info['date'],
            title = page_info['title'],
            version = page_info['version'],
            image_source_file = node.get_name()[1:] + '.png')

    # to save the results
    with open("./doc/" + 'node_' + node.get_name()[1:] + ".tex", "wb") as fh:
        fh.write(output_from_parsed_template.replace('&nbsp',' ').replace('</br>', '').replace('_', '\_'))


def __config_file_page__(config_file, node_names, page_info):
    env = Environment(loader=FileSystemLoader('rid/templates'))
    template = env.get_template('config_file.tex')
    f = open(config_file, 'r')
    text = f.read()
    #text = text.replace('\n', '</br>\n')
    output_from_parsed_template = template.render(
                name = config_file[1:],
                nodes = node_names,
                content = text.replace('_', 'a1s2d3f4'),
                nodes_list = page_info['nodes_list'],
                config_files_list = page_info['config_files_list'],
                topics_list = page_info['topics_list'],
                services_list = page_info['services_list'],
                date = page_info['date'],
                title = page_info['title'],
                version = page_info['version'])

    # to save the results
    with open("./doc/" + "config_" + config_file.replace('/', '_').replace('.', '_')[2:] + '.tex', "wb") as fh:
        fh.write(output_from_parsed_template.replace('&nbsp',' ').replace('</br>', '').replace('_', '\_').replace('a1s2d3f4', '_'))


def __msg_type_page__(msg_type, node_names, page_info):
    env = Environment(loader=FileSystemLoader('rid/templates'))
    template = env.get_template('msg_type_file.tex')
    try:
        text = rosmsg.get_msg_text(msg_type)
    except:
        text = "Unknown type message"

    #text = text.replace('\n', '</br>\n')
    #text = text.replace('  ', '&nbsp;&nbsp;&nbsp;&nbsp;')
    output_from_parsed_template = template.render(
                name = msg_type,
                nodes = node_names,
                content = text.replace('_', 'a1s2d3f4'),
                nodes_list = page_info['nodes_list'],
                config_files_list = page_info['config_files_list'],
                topics_list = page_info['topics_list'],
                services_list = page_info['services_list'],
                date = page_info['date'],
                title = page_info['title'],
                version = page_info['version'])

    # to save the results
    with open("./doc/" + "msgs_" + msg_type.replace('/', '_') + '.tex', "wb") as fh:
        fh.write(output_from_parsed_template.replace('</br>', '').replace('_', '\_').replace('a1s2d3f4', '_'))


def __srv_type_page__(srv_type, node_names, page_info):
    env = Environment(loader=FileSystemLoader('rid/templates'))
    template = env.get_template('srv_type_file.tex')
    try:
        text = rosmsg.get_srv_text(srv_type)
    except:
        print 'Error extracting info for service: ', srv_type
        text = "Unable to read service information"

    #text = text.replace('\n', '</br>\n')
    #text = text.replace('  ', '&nbsp;&nbsp;&nbsp;&nbsp;')
    in_out = text.split('---')
    if len(in_out) == 2:
        output_from_parsed_template = template.render(
                    name = srv_type,
                    nodes = node_names,
                    input_params = in_out[0],
                    output_params = in_out[1],
                    nodes_list = page_info['nodes_list'],
                    config_files_list = page_info['config_files_list'],
                    topics_list = page_info['topics_list'],
                    services_list = page_info['services_list'],
                    date = page_info['date'],
                    title = page_info['title'],
                    version = page_info['version'])

        # to save the results
        with open("./doc/" + "srvs_" + srv_type.replace('/', '_') + '.tex', "wb") as fh:
            fh.write(output_from_parsed_template.replace('</br>', '').replace('_', '\_'))


def __digest__(nodes,
               subcribed_to_topic,
               publishes_topic,
               page_info,
               description):
    return
