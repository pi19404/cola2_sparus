#!/usr/bin/env python
"""
Created on Nov-2013

@author: Narcis Palomeras
"""

class LinkHTML(object):
    def __init__(self, topic, topic_type, href, dependences=[]):
        self.topic = topic
        self.topic_type = topic_type
        self.href = href
        self.dependences = dependences

    def __str__(self):
        ret = self.topic + '[' + self.topic_type + '] -> ' + self.href + '\n'
        for i in self.dependences:
            ret = ret + '    ' + i.__str__()

        return ret