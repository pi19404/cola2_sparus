#!/usr/bin/env python
"""
Created on Nov-2013

@author: Narcis Palomeras
"""

# ROS imports
import roslib
roslib.load_manifest('pose_ekf_slam')
import rosservice
from link_html import LinkHTML

PYTHON_DESCRIPTION_TAG_INIT = '"""@@'
PYTHON_DESCRIPTION_TAG_END = '@@"""'
CPP_DESCRIPTION_TAG_INIT = '/*@@'
CPP_DESCRIPTION_TAG_END = '@@*/'


class NodeInfo(object):
    def __init__(self, name, source_file, config_file,
                 publish=[], subscribe=[], service=[]):
        self.name = name
        self.source_file = source_file
        self.config_file = config_file
        self.set_publish(publish)
        self.set_subscribe(subscribe)
        self.set_service(service)
        self.description = self.parse_description()


    def set_publish(self, publish):
        self.publish = list()
        self.publish_type = list()
        self.actions_lib = list()
        self.actions_lib_type = list()

        # Separate actionlibs from messages & erase rosout
        for p in publish:
            if '/result' in p:
                self.actions_lib.append(p.split('/')[1])
                self.actions_lib_type.append(p.split(' [')[1])

        # __delete_from_list_if_contains__(publish, '/status')
        # __delete_from_list_if_contains__(publish, '/feedback')
        # __delete_from_list_if_contains__(publish, '/result')
        __delete_from_list_if_contains__(publish, '/rosout')

        [self.publish, self.publish_type] = __separate_topic_and_type__(publish)


    def set_subscribe(self, subscribe):
        self.subscribe = list()
        self.subscribe_type = list()

        # Delete actionlib message
        # __delete_from_list_if_contains__(subscribe, '/cancel')
        [self.subscribe, self.subscribe_type] = __separate_topic_and_type__(subscribe)


    def set_service(self, service):
        self.service = list()
        self.service_type =  list()
        __delete_from_list_if_contains__(service, '/get_loggers')
        __delete_from_list_if_contains__(service, '/set_logger')
        self.service = service
        self.service_type = __discover_service_type__(service)


    def get_name(self):
        return self.name


    def get_description(self):
        return self.description


    def get_config_file(self):
        return self.config_file


    def get_publish(self):
        return self.publish


    def get_subscribe(self):
        return self.subscribe


    def get_service(self):
        return self.service


    def get_msg_types(self):
        return self.publish_type + self.subscribe_type


    def get_srv_types(self):
        return self.service_type


    def get_topics(self):
        return list(set(self.publish) | set(self.subscribe))

    def get_topic_type(self, topic):
        if topic in self.publish:
            return self.publish_type[self.publish.index(topic)]
        elif topic in self.subscribe:
            return self.subscribe_type[self.subscribe.index(topic)]
        else:
            return 'error_page'


    def get_source_file_html(self):
        if self.source_file != None:
            return LinkHTML(self.source_file,
                            None,
                            self.source_file.replace('.', '_') + '.html')
        else:
            return None


    def get_config_file_html(self):
        if self.config_file != None:
            return LinkHTML(self.config_file[1:],
                            None,
                            self.config_file.replace('/', '_').replace('.', '_') + '.html')
        else:
            return None


    def get_publish_html(self):
        if len(self.publish) > 0:
            ret = list()
            for i in range(len(self.publish)):
                ret.append(LinkHTML(self.publish[i],
                                    self.publish_type[i],
                                    self.publish_type[i].replace('/', '_') + '.html'))
            return ret
        else:
            return None


    def get_subscribe_html(self):
        if len(self.subscribe) > 0:
            ret = list()
            for i in range(len(self.subscribe)):
                ret.append(LinkHTML(self.subscribe[i],
                                    self.subscribe_type[i],
                                    self.subscribe_type[i].replace('/', '_') + '.html'))
            return ret
        else:
            return None


    def get_service_html(self):
        if len(self.service) > 0:
            ret = list()
            for i in range(len(self.service)):
                ret.append(LinkHTML(self.service[i],
                                    self.service_type[i],
                                    self.service_type[i].replace('/', '_') + '.html'))
            return ret
        else:
            return None


    def get_actionlib_html(self):
        if len(self.actions_lib) > 0:
            ret = list()
            for i in range(len(self.actions_lib)):
                ret.append(LinkHTML(self.actions_lib[i],
                                    self.actions_lib_type[i],
                                    self.actions_lib_type[i].replace('/', '_') + '.html'))
            return ret
        else:
            return None

    def parse_description(self):
        if self.source_file[-2:] == 'py':
            tag_init = PYTHON_DESCRIPTION_TAG_INIT
            tag_end = PYTHON_DESCRIPTION_TAG_END
        elif self.source_file[-3:] == 'cpp':
            tag_init = CPP_DESCRIPTION_TAG_INIT
            tag_end = CPP_DESCRIPTION_TAG_END
        else:
            print 'Unknow source file extension for ', self.source_file
            tag_init = CPP_DESCRIPTION_TAG_INIT
            tag_end = CPP_DESCRIPTION_TAG_END

        f = open(self.source_file, 'r')
        code = f.read()
        init = code.find(tag_init)
        if init > 0:
            final = code[init:-1].find(tag_end)
            if final > 0:
                text = code[init + len(tag_init):final + init]
                text = text.replace('\n', '</br>\n')
                text = text.replace(' ', '&nbsp')
                return text
        return 'No description found'


    def __str__(self):
        ret = self.name + ':\n'
        ret = ret + '"' + self.description + '"\n'
        ret = ret + 'source_file: ' + self.source_file + '\n'
        ret = ret + 'config_file: ' + self.config_file + '\n'
        for i in range(len(self.publish)):
            ret = ret + '-> ' + self.publish[i] + ' [' + self.publish_type[i] + ']\n'
        for i in range(len(self.subscribe)):
            ret = ret + '<- ' + self.subscribe[i] + ' [' + self.subscribe_type[i] + ']\n'
        for i in range(len(self.service)):
            ret = ret + 's: ' + self.service[i] + ' [' + self.service_type[i] + ']\n'
        for i in range(len(self.actions_lib)):
            ret = ret + 'a: ' + self.actions_lib[i] + ' [' + self.actions_lib_type[i] + ']\n'
        return ret


def __delete_from_list_if_contains__(data, element):
    delete = True
    while delete:
        delete = False
        for p in data:
            if element in p:
                data.remove(p)
                delete = True


def __discover_service_type__(service):
    types = list()
    for s in service:
        types.append(rosservice.get_service_type(s))
    return types


def __separate_topic_and_type__(data):
    topics = list()
    types = list()
    for i in data:
        [topic, typ] = i.split(' [')
        topics.append(topic)
        types.append(typ)
    return [topics, types]

