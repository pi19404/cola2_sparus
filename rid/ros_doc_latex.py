#!/usr/bin/env python
"""
Created on Nov-2013

@author: Narcis Palomeras
"""

# ROS imports
import roslib
roslib.load_manifest('pose_ekf_slam')
import rospy
import rosnode
import datetime
from node_info import NodeInfo
from link_html import LinkHTML
import create_latex_pages as create_page

CREATE_DIGEST = True


def __extract_node_info__(node_info):
    # Find publishers
    i = 2
    found = False
    publish = list()
    while i < len(node_info) and not found:
        if node_info[i] == '':
            found = True
        else:
            publish.append(node_info[i][3:-1])
            i = i + 1

    # Find subscribers
    i = i + 2
    found = False
    subscribe = []
    while i < len(node_info) and not found:
        if node_info[i] == '':
            found = True
        else:
            subscribe.append(node_info[i][3:-1])
            i = i + 1

    # Find service
    i = i +2
    found = False
    service = []
    while i < len(node_info) and not found:
        if node_info[i] == '':
            found = True
        else:
            service.append(node_info[i][3:])
            i = i + 1

    for s in service:
        if 'logger' in s:
            service.remove(s)

    return [publish, subscribe, service]


def __read_config_file__():
    ret = list()
    if rospy.has_param('title'):
        title = rospy.get_param('title')
    if rospy.has_param('version'):
        version = rospy.get_param('version')
    if rospy.has_param('description'):
        description = rospy.get_param('description')

    if rospy.has_param('nodes'):
        nodes_names = rospy.get_param('nodes')
    for node in nodes_names:
        if rospy.has_param(node + '/source_file'):
            source_file = rospy.get_param(node + '/source_file')
        else:
            source_file = None
            print 'Error! Not found source file for node: ', node
        if rospy.has_param(node + '/config_file'):
            config_file = rospy.get_param(node + '/config_file')
        else:
            config_file = None
        ret.append(NodeInfo(node, source_file, config_file))

    return [ret, title, version, description]


def __nodes_with_config_file__(config_file, nodes):
    ret = []
    for n in nodes:
        if n.get_config_file() == config_file:
            ret.append(LinkHTML(n.get_name(),
                                None,
                                n.get_name()[1:] + '.html'))
    return ret


def __nodes_with_msg_type__(msg_type, nodes):
    ret = []
    for n in nodes:
        if msg_type in n.get_msg_types():
            ret.append(LinkHTML(n.get_name(),
                                None,
                                n.get_name()[1:] + '.html'))
    return ret


def __nodes_with_srv_type__(srv_type, nodes):
    ret = []
    for n in nodes:
        if srv_type in n.get_srv_types():
            ret.append(LinkHTML(n.get_name(),
                                None,
                                n.get_name()[1:] + '.html'))
    return ret


def __get_nodes_list__(nodes):
    ret = []
    for n in nodes:
        ret.append(LinkHTML(n.get_name(),
                            None,
                            n.get_name()[1:] + '.html'))
    return ret


def __get_config_file_list__(nodes):
    ret = []
    for n in nodes:
        if n.get_config_file() != None:
            ret.append(LinkHTML(n.get_config_file(),
                                None,
                                n.get_config_file().replace('/', '_').replace('.', '_') + '.html'))
    return ret


def __get_topics_list__(nodes, publishes_topic):
    topics_tmp = list()
    for n in nodes:
        topics_tmp = list(set(n.get_publish()) | set(topics_tmp))

    ret = list()
    for topic in topics_tmp:
        if topic in publishes_topic:
            ret.append(LinkHTML(topic, None, publishes_topic[topic][0][1:] + '.html'))
    return ret


def __get_service_list__(nodes):
    ret = list()
    for n in nodes:
        for p in n.get_service():
            ret.append(LinkHTML(p, None, n.get_name()[1:] + '.html'))
    return ret

def __get_subscribers_for__(topic, nodes):
    ret = list()
    for n in nodes:
        if topic in n.get_subscribe():
            ret.append(n.get_name())

    return ret


def __get_publishers_for__(topic, nodes):
    ret = list()
    for n in nodes:
        if topic in n.get_publish():
            ret.append(n.get_name())

    return ret


if __name__ == '__main__':
    # read config file
    [nodes, title, version, description] = __read_config_file__()

    # Gather information
    for n in nodes:
        info = rosnode.get_node_info_description(n.get_name())
        info_split = info.split('\n')
        [publish, subscribe, service] = __extract_node_info__(info_split)
        n.set_publish(publish)
        n.set_subscribe(subscribe)
        n.set_service(service)

    # Look for publish --> subscribe and subscribe <-- publish info
    subcribed_to_topic = dict()
    publishes_topic = dict()

    topics_tmp = list()
    for n in nodes:
        topics_tmp = list(set(n.get_publish()) | set(topics_tmp))

    for topic in topics_tmp:
        subcribed_to_topic[topic] = __get_subscribers_for__(topic, nodes)

    topics_tmp = list()
    for n in nodes:
        topics_tmp = list(set(n.get_subscribe()) | set(topics_tmp))

    for topic in topics_tmp:
        publishes_topic[topic] = __get_publishers_for__(topic, nodes)


    # Create web

    # Get navigation menu info
    nodes_list = __get_nodes_list__(nodes)
    config_file_list = __get_config_file_list__(nodes)
    topics_list = __get_topics_list__(nodes, publishes_topic)
    service_list =  __get_service_list__(nodes)
    date = datetime.date.today().strftime('Created on %d, %b %Y by ROS \
                                            introspective documentator')
    page_info = dict()
    page_info['date'] = date
    page_info['nodes_list'] = nodes_list
    page_info['config_files_list'] = config_file_list
    page_info['topics_list'] = topics_list
    page_info['services_list'] = service_list
    page_info['title'] = title
    page_info['version'] = version
    page_info['description'] = description

    if CREATE_DIGEST:
        create_page.__digest__(nodes,
                       subcribed_to_topic,
                       publishes_topic,
                       page_info,
                       description)


    # Create web pages
    create_page.__index_page__("descripcio de la documentacio", page_info)

    for n in nodes:
        # Create node page
        create_page.__node_page__(n, subcribed_to_topic, publishes_topic, page_info)

        # Create Config File pages
        if n.get_config_file() != None:
            node_names = __nodes_with_config_file__(n.get_config_file(),
                                                    nodes)
            create_page.__config_file_page__(n.get_config_file(),
                                             node_names,
                                             page_info)

        # Create Msg type pages
        msg_types = n.get_msg_types()
        if msg_types > 0:
            for m in msg_types:
                node_names = __nodes_with_msg_type__(m, nodes)
                create_page.__msg_type_page__(m, node_names, page_info)


        # Create Srv type pages
        srv_types = n.get_srv_types()
        if srv_types > 0:
            for m in srv_types:
                node_names = __nodes_with_srv_type__(m, nodes)
                create_page.__srv_type_page__(m, node_names, page_info)
