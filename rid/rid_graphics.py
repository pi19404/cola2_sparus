# -*- coding: utf-8 -*-
"""
Created on Sat Nov  9 23:32:56 2013

@author: narcis palomeras
"""

import pydot

def __create_node_graph__(node_name, publish_html, subscribe_html):
    graph = pydot.Dot(graph_type='digraph')
    node = pydot.Node(node_name,
                      style="filled",
                      fillcolor="#6060FF")
    graph.add_node(node)

    if publish_html is not None:
        for i in publish_html:
            graph.add_node(pydot.Node(i.topic))
            graph.add_edge(pydot.Edge(node_name, i.topic))
            if i.dependences is not None:
                for j in i.dependences:
                    graph.add_node(pydot.Node(j.topic,
                                              style="filled",
                                              fillcolor="#F2F2FF"))
                    graph.add_edge(pydot.Edge(i.topic, j.topic))

    if subscribe_html is not None:
        for i in subscribe_html:
            graph.add_node(pydot.Node(i.topic))
            graph.add_edge(pydot.Edge(i.topic, node_name))
            if i.dependences is not None:
                for j in i.dependences:
                    graph.add_node(pydot.Node(j.topic,
                                              style="filled",
                                              fillcolor="#F2F2FF"))
                    graph.add_edge(pydot.Edge(j.topic, i.topic))

    # Overwrite it because loops can change their parameters
    node = pydot.Node(node_name,
                      style="filled",
                      fillcolor="#6060FF")
    graph.add_node(node)

    graph.write_png('./doc' + node_name + '.png')

