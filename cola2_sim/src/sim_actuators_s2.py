#!/usr/bin/env python
"""@@This node is used to simulate Sparus II actuators. It is only used in simulation.@@"""

# Basic ROS imports
import roslib
roslib.load_manifest('cola2_sim')
import rospy

# Import msgs
from cola2_control.msg import Setpoints

# More imports
import numpy as np


class SimActuatorsS2 :
    """ Simulates thrusters and fins of Sparus2 AUV, converting from setpoints
        to angles or rpm """

    def __init__(self, name):
        """ Constructor """
        self.name = name

        self.last_trusters_update = rospy.Time().now()

        # Create subscribers
        rospy.Subscriber('/cola2_control/thrusters_data',
                         Setpoints,
                         self.update_thrusters)
        rospy.Subscriber('/cola2_control/fins_data',
                         Setpoints,
                         self.update_fins)

        # Create publishers
        self.pub_thrusters = rospy.Publisher(
                                   '/cola2_control/sim_thrusters_data',
                                   Setpoints)
        self.pub_fins = rospy.Publisher('/cola2_control/sim_fins_data',
                                        Setpoints)

        # Timer
        rospy.Timer(rospy.Duration(1.0), self.check_thrusters)

        # Show message
        rospy.loginfo("%s: initialized", self.name)


    def update_thrusters(self, thrusters) :
        """ Thrusters callback """
        self.last_trusters_update = rospy.Time().now()
        msg = Setpoints()
        msg.header = thrusters.header
        # Linear approach with max rpm of 1200
        msg.setpoints = 1200 * np.array(thrusters.setpoints).clip(min=-1,
                                                                  max=1)
        self.pub_thrusters.publish(msg)


    def check_thrusters(self, event):
        """ Method used to tell dynamics node that thrusters stopped due to
            not receiving data """
        if (rospy.Time().now() - self.last_trusters_update).to_sec() > 2.0:
            msg = Setpoints()
            msg.setpoints = np.zeros(3)  # Number of thrusters here
            self.pub_thrusters.publish(msg)


    def update_fins(self, fins) :
        """ Fins callback """
        msg = Setpoints()
        msg.header = fins.header
        # Linear approach with max angle of 60 degrees
        msg.setpoints = 1.0471975512 * np.array(fins.setpoints).clip(min=-1,
                                                                     max=1)
        self.pub_fins.publish(msg)


if __name__ == '__main__':
    try:
        rospy.init_node('sim_actuators_s2')
        sim_actuators_s2 = SimActuatorsS2(rospy.get_name())
        rospy.spin()

    except rospy.ROSInterruptException:
        pass

