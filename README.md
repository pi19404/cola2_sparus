# COLA2

The Component Oriented Layered-base Architecture for Autonomy (COLA2) is 
the architecture developed in the Research Center of Underwater Robotics 
(CIRS) in the University of Girona (UdG). 
This architecture is used to control the Autonomous Underwater Vehicles (AUVs) 
developed in this center. Nowadays, Girona500 AUV and Sparus I/II AUVs are 
using this architecture.

*Read LICENSE file for terms and conditions*

